/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <couchdb-glib.h>
#include <utils.h>

static CouchdbSession *couchdb;

static CouchdbArrayField *
create_fake_array (void)
{
	CouchdbArrayField *array;
	gint i;

	array = couchdb_array_field_new ();
	for (i = 0; i < 10; i++)
		couchdb_array_field_add_int_element (array, i);

	return array;
}

static void
test_array_field (void)
{
	CouchdbArrayField *array, *tmp_array;

	array = couchdb_array_field_new ();
	g_assert (COUCHDB_IS_ARRAY_FIELD (array));

	tmp_array = create_fake_array ();
	couchdb_array_field_add_array_element (array, tmp_array);
	g_assert (couchdb_array_field_get_length (couchdb_array_field_get_array_element (array, couchdb_array_field_get_length (array) - 1))
		  == couchdb_array_field_get_length (tmp_array));
	g_object_unref (G_OBJECT (tmp_array));

	couchdb_array_field_add_boolean_element (array, TRUE);
	g_assert (couchdb_array_field_get_boolean_element (array, couchdb_array_field_get_length (array) - 1) == TRUE);

	couchdb_array_field_add_double_element (array, 1.0);
	g_assert (couchdb_array_field_get_double_element (array, couchdb_array_field_get_length (array) - 1) == 1.0);

	couchdb_array_field_add_int_element (array, 1);
	g_assert (couchdb_array_field_get_int_element (array, couchdb_array_field_get_length (array) - 1) == 1);

	couchdb_array_field_add_string_element (array, "hola");
	g_assert (g_strcmp0 (couchdb_array_field_get_string_element (array, couchdb_array_field_get_length (array) - 1),
			     "hola") == 0);

	do {
		gint length = couchdb_array_field_get_length (array);

		couchdb_array_field_remove_element (array, 0);
		g_assert (couchdb_array_field_get_length (array) == length - 1);
	} while (couchdb_array_field_get_length (array) > 0);

	g_object_unref (G_OBJECT (array));
}

static void
test_struct_field (void)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new ();
	g_assert (COUCHDB_IS_STRUCT_FIELD (sf));

	couchdb_struct_field_set_boolean_field (sf, "boolean", TRUE);
	g_assert (couchdb_struct_field_get_boolean_field (sf, "boolean") == TRUE);

	couchdb_struct_field_set_double_field (sf, "double", 1.0);
	g_assert (couchdb_struct_field_get_double_field (sf, "double") == 1.0);

	couchdb_struct_field_set_int_field (sf, "int", 1);
	g_assert (couchdb_struct_field_get_int_field (sf, "int") == 1);

	couchdb_struct_field_set_string_field (sf, "string", "hola");
	g_assert (g_strcmp0 (couchdb_struct_field_get_string_field (sf, "string"), "hola") == 0);

	g_object_unref (G_OBJECT (sf));
}

static void
test_list_databases (void)
{
	GError *error = NULL;
	GSList *dblist;

	dblist = couchdb_session_list_databases (couchdb, &error);
	if (error != NULL) {
		/* A critical will abort the test case */
		g_critical ("Error listing databases: %s", error->message);
		g_error_free (error);
		error = NULL;		
	}

	while (dblist != NULL) {
		CouchdbDatabaseInfo *dbinfo;
		CouchdbDatabase *database;
		GSList *doclist;

		error = NULL;
		dbinfo = couchdb_session_get_database_info (couchdb, (const char *) dblist->data, &error);
		g_assert (error == NULL);
		g_assert (dbinfo != NULL);
		g_assert (couchdb_database_info_get_dbname (dbinfo) != NULL);

		error = NULL;
		database = couchdb_session_get_database (couchdb, (const char *) dblist->data, &error);
		g_assert (error == NULL);
		g_assert (database != NULL);

		/* Get list of documents to compare against couchdb_database_info_get_documents_count */
		error = NULL;
		doclist = couchdb_database_get_all_documents (database, &error);
		g_assert (error == NULL);
		g_assert (g_slist_length (doclist) == couchdb_database_info_get_documents_count (dbinfo));
		if (doclist)
			couchdb_database_free_document_list (doclist);

		dblist = g_slist_remove (dblist, dblist->data);
		couchdb_database_info_unref (dbinfo);
		g_object_unref (G_OBJECT (database));
	}
}

static void
test_list_documents (void)
{
	GError *error = NULL;
	GSList *dblist;

	dblist = couchdb_session_list_databases (couchdb, &error);
	g_assert (error == NULL);

	while (dblist != NULL) {
		GSList *doclist;
		CouchdbDatabase *database;

		error = NULL;
		database = couchdb_session_get_database (couchdb, (const char *) dblist->data, &error);
		g_assert (error == NULL);
		g_assert (database != NULL);

		error = NULL;
		doclist = couchdb_database_get_all_documents (database, &error);
		g_assert (error == NULL);

		while (doclist) {
			CouchdbDocument *document = COUCHDB_DOCUMENT (doclist->data);
			char *str;

			str = couchdb_document_to_string (document);
			g_assert (str != NULL);
			g_free (str);

			doclist = g_slist_remove (doclist, document);
			g_object_unref (document);
		}

		dblist = g_slist_remove (dblist, dblist->data);
		g_object_unref (G_OBJECT (database));
	}
}

static void
doc_changed_cb (CouchdbDatabase *database, CouchdbDocument *document, gpointer user_data)
{
	char *doc_str;

	doc_str = couchdb_document_to_string (document);
	g_print ("Document %s has been %s: %s\n",
		 couchdb_document_get_id (document),
		 (const gchar *) user_data,
		 doc_str);

	g_free (doc_str);
}

static void
doc_deleted_cb (CouchdbDatabase *database, const char *docid, gpointer user_data)
{
	g_print ("Document %s in database %s has been deleted\n", docid, couchdb_database_get_name (database));
}

static void
test_change_databases (void)
{
	char *dbname;
	gint i;
	GError *error = NULL;
	CouchdbDatabase *database;

	dbname = generate_uuid ();
	g_assert (dbname != NULL);

	/* Database name can not start with a digit
	 * we will overwrite the first character with 'a' */
	 dbname[0] = 'a';

	/* Create database */
	couchdb_session_create_database (couchdb, dbname, &error);
	if (error) {
		g_critical ("Error creating database '%s': %s", dbname, error->message);
		g_error_free (error);
	}

	database = couchdb_session_get_database (couchdb, dbname, &error);
	g_signal_connect (G_OBJECT (database), "document_created", G_CALLBACK (doc_changed_cb), "created");
	g_signal_connect (G_OBJECT (database), "document_updated", G_CALLBACK (doc_changed_cb), "updated");
	g_signal_connect (G_OBJECT (database), "document_deleted", G_CALLBACK (doc_deleted_cb), NULL);

	couchdb_database_listen_for_changes (database);

	/* Create some documents */
	for (i = 0; i < 10; i++) {
		CouchdbDocument *document;
		char *str;

		document = couchdb_document_new ();
		g_assert (document != NULL);

		couchdb_document_set_boolean_field (document, "boolean", TRUE);
		couchdb_document_set_int_field (document, "int", i);
		couchdb_document_set_double_field (document, "double", (gdouble) i);

		str = g_strdup_printf ("value%d", i);
		couchdb_document_set_string_field (document, "string", str);
		g_free (str);

		g_assert (couchdb_database_put_document (database, document, &error));
		g_assert (error == NULL);
	}

	g_object_unref (G_OBJECT (database));

	/* Delete database */
	g_assert (couchdb_session_delete_database (couchdb, dbname, &error));
	g_assert (error == NULL);

	/* Free memory */
	g_free (dbname);
}

static void
db_created_cb (CouchdbSession *couchdb, const char *dbname, gpointer user_data)
{
	g_print ("Database %s has been created\n", dbname);
}

static void
db_deleted_cb (CouchdbSession *couchdb, const char *dbname, gpointer user_data)
{
	g_print ("Database %s has been deleted\n", dbname);
}

int
main (int argc, char *argv[])
{
	g_type_init ();
	g_thread_init (NULL);
	g_test_init (&argc, &argv, NULL);

	/* Initialize data needed for all tests */
	couchdb =  argc > 1 ? couchdb_session_new (argv[1]) : couchdb_session_new ("http://test:test@127.0.0.1:5985");
	g_print ("Connecting to Couchdb at %s\n", couchdb_session_get_uri (couchdb));
	
	if (!couchdb) {
		g_print ("Could not create Couchdb object\n");
		return -1;
	}

	g_signal_connect (G_OBJECT (couchdb), "database_created", G_CALLBACK (db_created_cb), NULL);
	g_signal_connect (G_OBJECT (couchdb), "database_deleted", G_CALLBACK (db_deleted_cb), NULL);

	/* Setup test functions */
	g_test_add_func ("/testcouchdbglib/TestArrayField", test_array_field);
	g_test_add_func ("/testcouchdbglib/TestStructField", test_struct_field);
	g_test_add_func ("/testcouchdbglib/ListDatabases", test_list_databases);
	g_test_add_func ("/testcouchdbglib/ListDocuments", test_list_documents);
	g_test_add_func ("/testcouchdbglib/ChangeDatabases", test_change_databases);

	return g_test_run ();
}
