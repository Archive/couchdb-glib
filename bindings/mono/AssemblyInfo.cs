using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Security;


[assembly: AssemblyCopyright("Copyright 2010, Rodrigo Moya")]
[assembly: AssemblyDescription("couchdb-glib binding for Mono and .NET")]
[assembly: AssemblyVersion("1.0.0.0")]
