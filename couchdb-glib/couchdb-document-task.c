/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2011 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Miguel Angel Rodelas Delgado <miguel.rodelas@gmail.com>
 *          Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "couchdb-document-task.h"
#include "utils.h"

G_DEFINE_TYPE(CouchdbDocumentTask, couchdb_document_task, COUCHDB_TYPE_DOCUMENT)

static void
couchdb_document_task_class_init (CouchdbDocumentTaskClass *klass)
{
}

static void
couchdb_document_task_init (CouchdbDocumentTask *document)
{
	couchdb_document_set_record_type (COUCHDB_DOCUMENT (document), COUCHDB_RECORD_TYPE_TASK);
}

/**
 * couchdb_document_task_new:
 *
 * Create a new #CouchdbDocumentTask object.
 */
CouchdbDocumentTask *
couchdb_document_task_new (void)
{
	return g_object_new (COUCHDB_TYPE_DOCUMENT_TASK, NULL);
}

const char *
couchdb_document_task_get_summary (CouchdbDocumentTask *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_TASK (document), NULL);
	g_return_val_if_fail (couchdb_document_is_task (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "summary");
}

void
couchdb_document_task_set_summary (CouchdbDocumentTask *document, const char *summary)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_TASK (document));
	g_return_if_fail (couchdb_document_is_task (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (summary != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "summary", summary);
}

const char *
couchdb_document_task_get_description (CouchdbDocumentTask *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_TASK (document), NULL);
	g_return_val_if_fail (couchdb_document_is_task (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "description");
}

void
couchdb_document_task_set_description (CouchdbDocumentTask *document, const char *description)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_TASK (document));
	g_return_if_fail (couchdb_document_is_task (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (description != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "description", description);
}
