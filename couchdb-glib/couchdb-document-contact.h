/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COUCHDB_DOCUMENT_CONTACT_H__
#define __COUCHDB_DOCUMENT_CONTACT_H__

#include <couchdb-struct-field.h>
#include <couchdb-document.h>

G_BEGIN_DECLS

#define COUCHDB_TYPE_DOCUMENT_CONTACT                (couchdb_document_contact_get_type ())
#define COUCHDB_DOCUMENT_CONTACT(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), COUCHDB_TYPE_DOCUMENT_CONTACT, CouchdbDocumentContact))
#define COUCHDB_IS_DOCUMENT_CONTACT(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), COUCHDB_TYPE_DOCUMENT_CONTACT))
#define COUCHDB_DOCUMENT_CONTACT_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), COUCHDB_TYPE_DOCUMENT_CONTACT, CouchdbDocumentContactClass))
#define COUCHDB_IS_DOCUMENT_CONTACT_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), COUCHDB_TYPE_DOCUMENT_CONTACT))
#define COUCHDB_DOCUMENT_CONTACTGET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), COUCHDB_TYPE_DOCUMENT_CONTACT, CouchdbDocumentContactClass))

typedef struct {
	CouchdbDocument parent;
} CouchdbDocumentContact;

typedef struct {
	CouchdbDocumentClass parent_class;
} CouchdbDocumentContactClass;

GType       couchdb_document_contact_get_type (void);

CouchdbDocumentContact *couchdb_document_contact_new (void);

/*
 * Top level functions to manipulate documents representing a contact
 */

const char *couchdb_document_contact_get_title (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_title (CouchdbDocumentContact *document, const char *title);
const char *couchdb_document_contact_get_first_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_first_name (CouchdbDocumentContact *document, const char *first_name);
const char *couchdb_document_contact_get_middle_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_middle_name (CouchdbDocumentContact *document, const char *middle_name);
const char *couchdb_document_contact_get_last_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_last_name (CouchdbDocumentContact *document, const char *last_name);
const char *couchdb_document_contact_get_suffix (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_suffix (CouchdbDocumentContact *document, const char *suffix);

const char *couchdb_document_contact_get_nick_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_nick_name (CouchdbDocumentContact *document, const char *nick_name);
const char *couchdb_document_contact_get_spouse_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_spouse_name (CouchdbDocumentContact *document, const char *spouse_name);
const char *couchdb_document_contact_get_birth_date (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_birth_date (CouchdbDocumentContact *document, const char *birth_date);
const char *couchdb_document_contact_get_wedding_date (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_wedding_date (CouchdbDocumentContact *document, const char *wedding_date);

const char *couchdb_document_contact_get_company (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_company (CouchdbDocumentContact *document, const char *company);
const char *couchdb_document_contact_get_department (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_department (CouchdbDocumentContact *document, const char *department);
const char *couchdb_document_contact_get_job_title (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_job_title (CouchdbDocumentContact *document, const char *job_title);
const char *couchdb_document_contact_get_manager_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_manager_name (CouchdbDocumentContact *document, const char *manager_name);
const char *couchdb_document_contact_get_assistant_name (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_assistant_name (CouchdbDocumentContact *document, const char *assistant_name);
const char *couchdb_document_contact_get_office (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_office (CouchdbDocumentContact *document, const char *office);

GSList     *couchdb_document_contact_get_email_addresses (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_email_addresses (CouchdbDocumentContact *document, GSList *list);

GSList     *couchdb_document_contact_get_phone_numbers (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_phone_numbers (CouchdbDocumentContact *document, GSList *list);

GSList     *couchdb_document_contact_get_addresses (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_addresses (CouchdbDocumentContact *document, GSList *list);

GSList     *couchdb_document_contact_get_im_addresses (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_im_addresses (CouchdbDocumentContact *document, GSList *list);

GSList     *couchdb_document_contact_get_urls (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_urls (CouchdbDocumentContact *document, GSList *list);

const char *couchdb_document_contact_get_categories (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_categories (CouchdbDocumentContact *document, const char *categories);

const char *couchdb_document_contact_get_notes (CouchdbDocumentContact *document);
void        couchdb_document_contact_set_notes (CouchdbDocumentContact *document, const char *notes);

/*
 * Utility functions to manipulate email addresses fields
 */

CouchdbStructField *couchdb_document_contact_email_new (const char *uuid, const char *address, const char *description);
const char         *couchdb_document_contact_email_get_address (CouchdbStructField *sf);
void                couchdb_document_contact_email_set_address (CouchdbStructField *sf, const char *email);

#define COUCHDB_DOCUMENT_CONTACT_EMAIL_DESCRIPTION_HOME  "home"
#define COUCHDB_DOCUMENT_CONTACT_EMAIL_DESCRIPTION_OTHER "other"
#define COUCHDB_DOCUMENT_CONTACT_EMAIL_DESCRIPTION_WORK  "work"

const char         *couchdb_document_contact_email_get_description (CouchdbStructField *sf);
void                couchdb_document_contact_email_set_description (CouchdbStructField *sf, const char *description);

/*
 * Utility functions to manipulate phone numbers
 */

CouchdbStructField *couchdb_document_contact_phone_new (const char *uuid, const char *number, const char *description, gint priority);
gint                couchdb_document_contact_phone_get_priority (CouchdbStructField *sf);
void                couchdb_document_contact_phone_set_priority (CouchdbStructField *sf, gint priority);
const char         *couchdb_document_contact_phone_get_number (CouchdbStructField *sf);
void                couchdb_document_contact_phone_set_number (CouchdbStructField *sf, const char *number);

#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_ASSISTANT "assistant"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_CALLBACK  "callback"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_CAR       "car"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_COMPANY   "company"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_HOME      "home"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_HOME_FAX  "home fax"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_MOBILE    "mobile"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_OTHER     "other"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_OTHER_FAX "other fax"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_PAGER     "pager"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_PRIMARY   "primary"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_RADIO     "radio"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_TELEX     "telex"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_WORK      "work"
#define COUCHDB_DOCUMENT_CONTACT_PHONE_DESCRIPTION_WORK_FAX  "work fax"

const char         *couchdb_document_contact_phone_get_description (CouchdbStructField *sf);
void                couchdb_document_contact_phone_set_description (CouchdbStructField *sf, const char *description);

/*
 * Utility functions to manipulate addresses
 */
CouchdbStructField *couchdb_document_contact_address_new (const char *uuid,
							       const char *street,
							       const char *ext_street,
							       const char *city,
							       const char *state,
							       const char *country,
							       const char *postalcode,
							       const char *pobox,
							       const char *description);
const char         *couchdb_document_contact_address_get_street (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_street (CouchdbStructField *sf, const char *street);
const char         *couchdb_document_contact_address_get_ext_street (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_ext_street (CouchdbStructField *sf, const char *ext_street);
const char         *couchdb_document_contact_address_get_city (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_city (CouchdbStructField *sf, const char *city);
const char         *couchdb_document_contact_address_get_state (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_state (CouchdbStructField *sf, const char *state);
const char         *couchdb_document_contact_address_get_country (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_country (CouchdbStructField *sf, const char *country);
const char         *couchdb_document_contact_address_get_postalcode (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_postalcode (CouchdbStructField *sf, const char *postalcode);
const char         *couchdb_document_contact_address_get_pobox (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_pobox (CouchdbStructField *sf, const char *pobox);

#define COUCHDB_DOCUMENT_CONTACT_ADDRESS_DESCRIPTION_HOME  "home"
#define COUCHDB_DOCUMENT_CONTACT_ADDRESS_DESCRIPTION_OTHER "other"
#define COUCHDB_DOCUMENT_CONTACT_ADDRESS_DESCRIPTION_WORK  "work"

const char         *couchdb_document_contact_address_get_description (CouchdbStructField *sf);
void                couchdb_document_contact_address_set_description (CouchdbStructField *sf, const char *description);

/*
 * Utility functions to manipulate IM addresses
 */
CouchdbStructField *couchdb_document_contact_im_new (const char *uuid,
						     const char *address,
						     const char *description,
						     const char *protocol);
const char         *couchdb_document_contact_im_get_address (CouchdbStructField *sf);
void                couchdb_document_contact_im_set_address (CouchdbStructField *sf, const char *address);
const char         *couchdb_document_contact_im_get_description (CouchdbStructField *sf);
void                couchdb_document_contact_im_set_description (CouchdbStructField *sf, const char *description);

#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_AIM       "aim"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_GADU_GADU "gadu-gadu"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_GROUPWISE "groupwise"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_ICQ       "icq"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_IRC       "irc"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_JABBER    "jabber"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_MSN       "msn"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_SKYPE     "skype"
#define COUCHDB_DOCUMENT_CONTACT_IM_PROTOCOL_YAHOO     "yahoo"

const char         *couchdb_document_contact_im_get_protocol (CouchdbStructField *sf);
void                couchdb_document_contact_im_set_protocol (CouchdbStructField *sf, const char *protocol);

/*
 * Utility functions to manipulate URLs
 */
CouchdbStructField *couchdb_document_contact_url_new (const char *uuid, const char *address, const char *description);
const char         *couchdb_document_contact_url_get_address  (CouchdbStructField *sf);
void                couchdb_document_contact_url_set_address (CouchdbStructField *sf, const char *address);

#define COUCHDB_DOCUMENT_CONTACT_URL_DESCRIPTION_BLOG     "blog"
#define COUCHDB_DOCUMENT_CONTACT_URL_DESCRIPTION_HOMEPAGE "home page"

const char         *couchdb_document_contact_url_get_description (CouchdbStructField *sf);
void                couchdb_document_contact_url_set_description (CouchdbStructField *sf, const char *description);

G_END_DECLS

#endif /* __COUCHDB_DOCUMENT_CONTACT_H__ */
