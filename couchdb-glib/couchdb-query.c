/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Krzysztof Klimonda
 *
 * Authors: Krzysztof Klimonda <kklimonda@syntaxhighlighted.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include <libsoup/soup-uri.h>

#include "couchdb-query.h"
#include "utils.h"

#define GET_PRIVATE(obj)				\
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj),			\
				COUCHDB_TYPE_QUERY,	\
				CouchdbQueryPrivate))

G_DEFINE_TYPE(CouchdbQuery, couchdb_query, G_TYPE_OBJECT)

struct _CouchdbQueryPrivate {
	GHashTable *query_options;
	gchar *query_string;
	JsonObject *body;
	gchar *path;
	gchar *method;
};

enum {
	PROP_0,

	PROP_QUERY_OPTIONS,
	PROP_PATH
};

static void
couchdb_query_get_property (GObject *object, guint property_id,
			    GValue *value, GParamSpec *pspec)
{
	CouchdbQuery *self;

	self = COUCHDB_QUERY (object);

	switch (property_id) {
	case PROP_QUERY_OPTIONS:
		g_value_set_boxed (value, self->priv->query_options);
		break;
	case PROP_PATH:
		g_value_set_string (value, self->priv->path);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
couchdb_query_set_property (GObject *object, guint property_id,
			    const GValue *value, GParamSpec *pspec)
{
	CouchdbQuery *self;

	self = COUCHDB_QUERY (object);

	switch (property_id) {
	case PROP_QUERY_OPTIONS:
		if (self->priv->query_options)
			g_hash_table_destroy (self->priv->query_options);
		self->priv->query_options = g_value_get_boxed (value);
		break;
	case PROP_PATH:
		g_free (self->priv->path);
		self->priv->path = g_strdup (g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
couchdb_query_dispose (GObject *object)
{
	CouchdbQuery *self = COUCHDB_QUERY (object);

	g_hash_table_destroy (self->priv->query_options);

	if (self->priv->body)
		json_object_unref (self->priv->body);

 	g_free (self->priv->query_string);
	g_free (self->priv->path);
	g_free (self->priv->method);

	G_OBJECT_CLASS (couchdb_query_parent_class)->dispose (object);
}

static void
couchdb_query_class_init (CouchdbQueryClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec;

	gobject_class->dispose = couchdb_query_dispose;
	gobject_class->set_property = couchdb_query_set_property;
	gobject_class->get_property = couchdb_query_get_property;

	g_type_class_add_private (klass, sizeof (CouchdbQueryPrivate));

	pspec = g_param_spec_boxed ("query-options",
				    "Query Options",
				    "Query options associated with the query",
				    G_TYPE_HASH_TABLE,
				    G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class,
					 PROP_QUERY_OPTIONS,
					 pspec);

	pspec = g_param_spec_string ("path",
				     "Query's Path",
				     "A path for the query.",
				     NULL,
				     G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class,
					 PROP_PATH,
					 pspec);

}

static void
couchdb_query_init (CouchdbQuery *self)
{
	self->priv = GET_PRIVATE (self);

	self->priv->query_options =
		g_hash_table_new_full (g_str_hash, g_str_equal,
				       g_free, g_free);
	self->priv->query_string = NULL;
	self->priv->body = NULL;
	self->priv->path = NULL;
	self->priv->method = g_strdup ("GET");
}

/**
 * couchdb_query_new:
 *
 * Creates and returns a new #CouchdbQuery object
 *
 * Return value: A new #CouchdbQuery object, to be unreferenced
 * when no longer needed.
 */
CouchdbQuery *
couchdb_query_new ()
{
	return g_object_new (COUCHDB_TYPE_QUERY, NULL);
}

/**
 * couchdb_query_new_for_path:
 * @path: a path for query
 *
 * Creates and returns a new #CouchdbQuery object for a given path
 *
 * Return value: A new #CouchdbQuery object, to be unreferenced
 * when no longer needed.
 */
CouchdbQuery *
couchdb_query_new_for_path (const gchar *path)
{
	return g_object_new (COUCHDB_TYPE_QUERY,
			     "path", path,
			     NULL);
}

/**
 * couchdb_query_new_for_view:
 * @design_doc: A document's ID. You can ommit the _design/ part if you want to.
 * @view_name: Name of the view to call.
 *
 * Creates and returns a new #CouchdbQuery object for a give design document
 * and view. It's a shortcut for this code:
 * [[[
 * const gchar *design_doc, *view_name;
 * CouchdbQuery *query;
 *
 * design_doc = "document";
 * view_name = "view";
 * path = g_str_concat ("_design/", design_doc, "_view", view_name);
 * query = couchdb_query_new ();
 * couchdb_query_set_path (query, path);
 * g_free (path);
 * ]]]
 *
 * Return value: A new #CouchdbQuery object, to be unreferenced
 * when no longer needed.
 */
CouchdbQuery *
couchdb_query_new_for_view (const gchar *design_doc, const gchar *view_name)
{
	CouchdbQuery *query;
	gchar *path;

	if (!g_str_has_prefix (design_doc, "_design")) {
		path = g_strdup_printf ("_design/%s/_view/%s",
					design_doc, view_name);
	} else {
		path = g_strdup_printf ("%s/_view/%s",
					design_doc, view_name);
	}

	query = g_object_new (COUCHDB_TYPE_QUERY,
			      "path", path,
			      NULL);

	g_free (path);

	return query;

}

/**
 * couchdb_query_get_option
 *
 * Returns the value of the given key, or %NULL if it wasn't set
 *
 * Return value: (transfer none) (allow-none): the value of the given key,
 * or %NULL if it hasn't been set.
 */
const char *
couchdb_query_get_option (CouchdbQuery *self, const gchar *name)
{
	g_return_val_if_fail (COUCHDB_IS_QUERY (self), NULL);

	return g_hash_table_lookup (self->priv->query_options, name);
}

/**
 * couchdb_query_set_option
 *
 * Set's the value of the given key, overwriting the current one if it's
 * already set
 */
void
couchdb_query_set_option (CouchdbQuery *self, const gchar *name,
			  const gchar *value)
{
	g_return_if_fail (COUCHDB_IS_QUERY (self));

	if (self->priv->query_string != NULL) {
		g_free (self->priv->query_string);
		self->priv->query_string = NULL;
	}

	g_hash_table_insert (self->priv->query_options,
			     g_strdup (name), g_strdup (value));
}

/**
 * couchdb_query_get_query_options_string
 * @self: A #CouchdbQuery object
 *
 * Returns options as a query string that can be used to create
 * a complete uri for querying server.
 *
 * Return value: A string containing all query options.
 */
const char *
couchdb_query_get_query_options_string (CouchdbQuery *self)
{
	GHashTableIter iter;
	gpointer key, value;
	GString *query_string;

	g_return_val_if_fail (COUCHDB_IS_QUERY (self), NULL);

	if (self->priv->query_string != NULL) {
		return self->priv->query_string;
	}

	query_string = g_string_new (NULL);

	g_hash_table_iter_init (&iter, (GHashTable *) self->priv->query_options);

	while (g_hash_table_iter_next (&iter, &key, &value)) {
		/* key, startkey and endkey must be a properly url
		 * encoded JSON values */
		if (g_str_equal (key, "key") || g_str_equal (key, "startkey") ||
		    g_str_equal (key, "endkey")) {
			char *tmp;

			tmp = couchdb_uri_encode (value);

			/* an array of values can be passed as a key, in this
			 * case don't add quotation marks around it */
			g_string_append_printf (query_string,
						*((char *) value) == '[' ?
						"%s=%s&" : "%s=\"%s\"&",
						(char *) key, (char *) tmp);

			g_free (tmp);
		} else {
			g_string_append_printf (query_string, "%s=%s&",
						(char *) key, (char *) value);
		}
	}

	/* remove trailing & */
	if (query_string->len > 0) {
		query_string =
			g_string_truncate (query_string, query_string->len-1);

		return g_string_free (query_string, FALSE);
	} else {
		g_string_free (query_string, TRUE);

		return NULL;
	}
}

/**
 * couchdb_query_get_json_object
 *
 * Returns the Json body of the query.
 *
 * Return value: (transfer none): A JsonObject set as a body
 * of a query.
 */
JsonObject *
couchdb_query_get_json_object (CouchdbQuery *self)
{
	g_return_val_if_fail (COUCHDB_IS_QUERY (self), NULL);

	return self->priv->body;
}

/**
 * couchdb_query_get_json_object
 *
 * Sets the given Json object as a body of the query that is going to be
 * send to the CouchDB server. It doesn't change the query's method so it has
 * to be set to either PUT or POST by calling #couchdb_query_set_method.
 *
 * Return value: (transfer full): A JsonObject to be set as a Query's body.
 */
void
couchdb_query_set_json_object (CouchdbQuery *self, JsonObject *body)
{
	g_return_if_fail (COUCHDB_IS_QUERY (self));

	if (self->priv->body)
		json_object_unref (self->priv->body);
	self->priv->body = json_object_ref (body);
}

/**
 * couchdb_query_set_path:
 * @self: A #CouchdbQuery object
 * @path: A path for query.
 *
 * Sets path for the Query.
 */
void
couchdb_query_set_path (CouchdbQuery *self, const char *path)
{
	g_return_if_fail (COUCHDB_IS_QUERY (self));

	g_free (self->priv->path);
	self->priv->path = g_strdup (path);
}

/**
 * couchdb_query_get_path
 * @self: A #CouchdbQuery object
 *
 * Returns path set for the query.
 *
 * Return value: string containing path set for the query.
 */
const char *
couchdb_query_get_path (CouchdbQuery *self)
{
	g_return_val_if_fail (COUCHDB_IS_QUERY (self), NULL);

	return (const gchar *) self->priv->path;
}

/**
 * couchdb_query_set_method
 * @self: A #CouchdbQuery object
 * @method: A method for query.
 *
 * Sets method for the Query.
 */
void
couchdb_query_set_method (CouchdbQuery *self, const gchar *method)
{
	g_return_if_fail (COUCHDB_IS_QUERY (self));

	g_free (self->priv->method);
	self->priv->method = g_strdup (method);
}

/**
 * couchdb_query_get_method
 * @self: A #CouchdbQuery object
 *
 * Returns method set for the query.
 *
 * Return value: string containing method set for the query.
 */
const char *
couchdb_query_get_method (CouchdbQuery *self)
{
	g_return_val_if_fail (COUCHDB_IS_QUERY (self), NULL);

	return (const gchar *) self->priv->method;
}
