/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2010 Canonical Services Ltd (www.canonical.com)
 *               2009 Mikkel Kamstrup Erlandsen
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *          Mikkel Kamstrup Erlandsen <mikkel.kamstrup@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COUCHDB_DOCUMENT_H__
#define __COUCHDB_DOCUMENT_H__

#include <glib.h>
#include <glib-object.h>
#include "couchdb-types.h"
#include "couchdb-array-field.h"
#include "couchdb-struct-field.h"

G_BEGIN_DECLS

#define COUCHDB_TYPE_DOCUMENT                (couchdb_document_get_type ())
#define COUCHDB_DOCUMENT(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), COUCHDB_TYPE_DOCUMENT, CouchdbDocument))
#define COUCHDB_IS_DOCUMENT(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), COUCHDB_TYPE_DOCUMENT))
#define COUCHDB_DOCUMENT_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), COUCHDB_TYPE_DOCUMENT, CouchdbDocumentClass))
#define COUCHDB_IS_DOCUMENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), COUCHDB_TYPE_DOCUMENT))
#define COUCHDB_DOCUMENT_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), COUCHDB_TYPE_DOCUMENT, CouchdbDocumentClass))

typedef struct _CouchdbDocumentPrivate CouchdbDocumentPrivate;

typedef struct {
	GObject parent;
	CouchdbDocumentPrivate *priv;
} CouchdbDocument;

typedef struct {
	GObjectClass parent_class;
} CouchdbDocumentClass;

GType               couchdb_document_get_type (void);

CouchdbDocument    *couchdb_document_new (void);

const char         *couchdb_document_get_id (CouchdbDocument *document);
void                couchdb_document_set_id (CouchdbDocument *document, const char *id);
const char         *couchdb_document_get_revision (CouchdbDocument *document);
void                couchdb_document_set_revision (CouchdbDocument *document, const char *revision);

gboolean            couchdb_document_has_field (CouchdbDocument *document, const char *field);
void                couchdb_document_remove_field (CouchdbDocument *document, const char *field);
GSList             *couchdb_document_get_field_names (CouchdbDocument *document);
GType               couchdb_document_get_field_type (CouchdbDocument *document, const char *field);

CouchdbArrayField  *couchdb_document_get_array_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_array_field (CouchdbDocument *document, const char *field, CouchdbArrayField *value);
gboolean            couchdb_document_get_boolean_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_boolean_field (CouchdbDocument *document, const char *field, gboolean value);
gint                couchdb_document_get_int_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_int_field (CouchdbDocument *document, const char *field, gint value);
gdouble             couchdb_document_get_double_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_double_field (CouchdbDocument *document, const char *field, gdouble value);
const char         *couchdb_document_get_string_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_string_field (CouchdbDocument *document, const char *field, const char *value);
CouchdbStructField *couchdb_document_get_struct_field (CouchdbDocument *document, const char *field);
void                couchdb_document_set_struct_field (CouchdbDocument *document, const char *field, CouchdbStructField *value);

GSList             *couchdb_document_list_attachments (CouchdbDocument *document);

char               *couchdb_document_to_string (CouchdbDocument *document);

/*
 * Methods to deal with standard documents from Freedesktop specification
 */
const char         *couchdb_document_get_record_type (CouchdbDocument *document);
void                couchdb_document_set_record_type (CouchdbDocument *document,
						      const char *record_type);

CouchdbStructField *couchdb_document_get_application_annotations (CouchdbDocument *document);
void                couchdb_document_set_application_annotations (CouchdbDocument *document,
								  CouchdbStructField *annotations);

#define COUCHDB_RECORD_TYPE_CONTACT "http://www.freedesktop.org/wiki/Specifications/desktopcouch/contact"
#define COUCHDB_RECORD_TYPE_TASK "http://www.freedesktop.org/wiki/Specifications/desktopcouch/task"

gboolean            couchdb_document_is_contact (CouchdbDocument *document);
gboolean            couchdb_document_is_task (CouchdbDocument *document);

G_END_DECLS

#endif /* __COUCHDB_DOCUMENT_H__ */
