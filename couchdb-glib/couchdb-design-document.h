/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COUCHDB_DESIGN_DOCUMENT_H__
#define __COUCHDB_DESIGN_DOCUMENT_H__

#include "couchdb-document.h"

G_BEGIN_DECLS

#define COUCHDB_TYPE_DESIGN_DOCUMENT                (couchdb_design_document_get_type ())
#define COUCHDB_DESIGN_DOCUMENT(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), COUCHDB_TYPE_DESIGN_DOCUMENT, CouchdbDesignDocument))
#define COUCHDB_IS_DESIGN_DOCUMENT(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), COUCHDB_TYPE_DESIGN_DOCUMENT))
#define COUCHDB_DESIGN_DOCUMENT_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), COUCHDB_TYPE_DESIGN_DOCUMENT, CouchdbDesignDocumentClass))
#define COUCHDB_IS_DESIGN_DOCUMENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), COUCHDB_TYPE_DESIGN_DOCUMENT))
#define COUCHDB_DESIGN_DOCUMENT_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), COUCHDB_TYPE_DESIGN_DOCUMENT, CouchdbDesignDocumentClass))

typedef struct  {
	CouchdbDocument parent;
} CouchdbDesignDocument;

typedef struct  {
	CouchdbDocumentClass parent_class;
} CouchdbDesignDocumentClass;

GType                  couchdb_design_document_get_type (void);

CouchdbDesignDocument *couchdb_design_document_new (void);

GSList                *couchdb_design_document_list_views (CouchdbDesignDocument *document);
void                   couchdb_design_document_free_views_list (GSList *list);
void                   couchdb_design_document_add_view (CouchdbDesignDocument *document,
							 const char *name,
							 const char *map_function,
							 const char *reduce_function);
void                   couchdb_design_document_delete_view (CouchdbDesignDocument *document,
							    const char *name);

typedef enum {
	COUCHDB_DESIGN_DOCUMENT_LANGUAGE_UNKNOWN,
	COUCHDB_DESIGN_DOCUMENT_LANGUAGE_JAVASCRIPT,
	COUCHDB_DESIGN_DOCUMENT_LANGUAGE_PYTHON
} CouchdbDesignDocumentLanguage;

CouchdbDesignDocumentLanguage couchdb_design_document_get_language (CouchdbDesignDocument *document);
void                          couchdb_design_document_set_language (CouchdbDesignDocument *document,
								    CouchdbDesignDocumentLanguage language);

G_END_DECLS

#endif
