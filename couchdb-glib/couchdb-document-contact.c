/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "couchdb-document-contact.h"
#include "utils.h"

G_DEFINE_TYPE(CouchdbDocumentContact, couchdb_document_contact, COUCHDB_TYPE_DOCUMENT)

static void
couchdb_document_contact_class_init (CouchdbDocumentContactClass *klass)
{
}

static void
couchdb_document_contact_init (CouchdbDocumentContact *document)
{
	couchdb_document_set_record_type (COUCHDB_DOCUMENT (document), COUCHDB_RECORD_TYPE_CONTACT);
}

/**
 * couchdb_document_contact_new:
 *
 * Create a new #CouchdbDocumentContact object.
 */
CouchdbDocumentContact *
couchdb_document_contact_new (void)
{
	return g_object_new (COUCHDB_TYPE_DOCUMENT_CONTACT, NULL);
}

const char *
couchdb_document_contact_get_title (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "title");
}

void
couchdb_document_contact_set_title (CouchdbDocumentContact *document, const char *title)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (title != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "title", title);
}

const char *
couchdb_document_contact_get_first_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "first_name");
}

void
couchdb_document_contact_set_first_name (CouchdbDocumentContact *document, const char *first_name)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (first_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "first_name", first_name);
}

const char *
couchdb_document_contact_get_middle_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "middle_name");
}

void
couchdb_document_contact_set_middle_name (CouchdbDocumentContact *document, const char *middle_name)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (middle_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "middle_name", middle_name);
}

const char *
couchdb_document_contact_get_last_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "last_name");
}

void
couchdb_document_contact_set_last_name (CouchdbDocumentContact *document, const char *last_name)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (last_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "last_name", last_name);
}

const char *
couchdb_document_contact_get_suffix (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "suffix");
}

void
couchdb_document_contact_set_suffix (CouchdbDocumentContact *document, const char *suffix)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (suffix != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "suffix", suffix);
}

const char *
couchdb_document_contact_get_nick_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "nick_name");
}

void
couchdb_document_contact_set_nick_name (CouchdbDocumentContact *document, const char *nick_name)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (nick_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "nick_name", nick_name);
}

const char *
couchdb_document_contact_get_spouse_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "spouse_name");
}

void
couchdb_document_contact_set_spouse_name (CouchdbDocumentContact *document, const char *spouse_name)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (spouse_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "spouse_name", spouse_name);
}

const char *
couchdb_document_contact_get_birth_date (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "birth_date");
}

void
couchdb_document_contact_set_birth_date (CouchdbDocumentContact *document, const char *birth_date)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (birth_date != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "birth_date", birth_date);
}

const char *
couchdb_document_contact_get_wedding_date (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "wedding_date");
}

void
couchdb_document_contact_set_wedding_date (CouchdbDocumentContact *document, const char *wedding_date)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (wedding_date != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "wedding_date", wedding_date);
}

const char *
couchdb_document_contact_get_company (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "company");
}

void
couchdb_document_contact_set_company (CouchdbDocumentContact *document, const char *company)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (company != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "company", company);
}

const char *
couchdb_document_contact_get_department (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "department");
}

void
couchdb_document_contact_set_department (CouchdbDocumentContact *document, const char *department)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (department != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "department", department);
}

const char *
couchdb_document_contact_get_job_title (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "job_title");
}

void
couchdb_document_contact_set_job_title (CouchdbDocumentContact *document, const char *job_title)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (job_title != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "job_title", job_title);
}

const char *
couchdb_document_contact_get_manager_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "manager_name");
}

void
couchdb_document_contact_set_manager_name (CouchdbDocumentContact *document, const char *manager_name)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (manager_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "manager_name", manager_name);
}

const char *
couchdb_document_contact_get_assistant_name (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "assistant_name");
}

void
couchdb_document_contact_set_assistant_name (CouchdbDocumentContact *document, const char *assistant_name)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (assistant_name != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "assistant_name", assistant_name);
}

const char *
couchdb_document_contact_get_office (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "office");
}

void
couchdb_document_contact_set_office (CouchdbDocumentContact *document, const char *office)
{
      	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (office != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "office", office);
}

static void
foreach_object_cb (JsonObject *object,
		  const char *member_name,
		  JsonNode *member_node,
		  gpointer user_data)
{
	GSList **list = (GSList **) user_data;

	if (json_node_get_node_type (member_node) == JSON_NODE_OBJECT) {
		CouchdbStructField *sf;

		sf = couchdb_struct_field_new_from_json_object (
			json_object_ref (json_node_get_object (member_node)));
		couchdb_struct_field_set_uuid (sf, member_name);
		*list = g_slist_prepend (*list, sf);
	}
}

/**
 * couchdb_document_contact_get_email_addresses:
 * @document: A #CouchdbDocumentContact object representing a contact
 *
 * Retrieve a list of email addresses from the specified contact document.
 * Email addresses are returned in a GSList of #CouchdbStructField objects,
 * which can be manipulated with the couchdb_document_contact_email_* functions
 * and freed with:
 *     g_slist_foreach (list, (GFunc) couchdb_struct_field_unref, NULL);
 *     g_slist_free (list);
 *
 * Returns: (element-type Couchdb.StructField) (transfer full): a #GSList of #CouchdbStructField objects.
 */
GSList *
couchdb_document_contact_get_email_addresses (CouchdbDocumentContact *document)
{
	GSList *list = NULL;
	JsonObject *addresses_json;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	if (json_object_has_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				    "email_addresses")) {
		addresses_json = json_object_get_object_member (
			couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)), "email_addresses");;
		if (addresses_json) {
			json_object_foreach_member (addresses_json,
						    (JsonObjectForeach) foreach_object_cb,
						    &list);
		}
	}

	return list;
}

void
couchdb_document_contact_set_email_addresses (CouchdbDocumentContact *document, GSList *list)
{
	GSList *l;
	JsonObject *addresses_json;

	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));

	addresses_json = json_object_new ();
	for (l = list; l != NULL; l = l->next) {
		const gchar *address_str;
		CouchdbStructField *sf = (CouchdbStructField *) l->data;

		address_str = couchdb_document_contact_email_get_address (sf);
		if (address_str) {
			JsonObject *this_address;

			this_address = json_object_new ();
			json_object_set_string_member (this_address, "address", address_str);
			json_object_set_string_member (this_address, "description",
						       couchdb_document_contact_email_get_description (sf));

			json_object_set_object_member (addresses_json,
						       couchdb_struct_field_get_uuid (sf),
						       this_address);
		}
	}

	json_object_set_object_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				       "email_addresses",
				       addresses_json);
}

/**
 * couchdb_document_contact_get_phone_numbers:
 * @document: A #CouchdbDocumentContact object
 *
 * Get the list of phone numbers for this contact document.
 *
 * Returns: (element-type Couchdb.StructField) (transfer full): A #GSList of
 * #CouchdbStructField's representing all the phone numbers stored for this
 * contact.
 */
GSList *
couchdb_document_contact_get_phone_numbers (CouchdbDocumentContact *document)
{
	GSList *list = NULL;
	JsonObject *phone_numbers;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	if (json_object_has_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				    "phone_numbers")) {
		phone_numbers = json_object_get_object_member (
			couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)), "phone_numbers");
		if (phone_numbers) {
			json_object_foreach_member (phone_numbers,
						    (JsonObjectForeach) foreach_object_cb,
						    &list);
		}
	}

	return list;
}

void
couchdb_document_contact_set_phone_numbers (CouchdbDocumentContact *document, GSList *list)
{
	GSList *l;
	JsonObject *phone_numbers;

	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));

	phone_numbers = json_object_new ();
	for (l = list; l != NULL; l = l->next) {
		const gchar *number_str;
		CouchdbStructField *sf = (CouchdbStructField *) l->data;

		number_str = couchdb_document_contact_phone_get_number (sf);
		if (number_str) {
			JsonObject *this_phone;

			this_phone = json_object_new ();
			json_object_set_string_member (this_phone, "number", number_str);
			json_object_set_string_member (this_phone, "description",
						       couchdb_document_contact_phone_get_description (sf));
			json_object_set_int_member (this_phone, "priority",
						    couchdb_document_contact_phone_get_priority (sf));

			json_object_set_object_member (phone_numbers,
						       couchdb_struct_field_get_uuid (sf),
						       this_phone);
		}
	}

	json_object_set_object_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				       "phone_numbers", phone_numbers);
}

/**
 * couchdb_document_contact_get_addresses:
 * @document: A #CouchdbDocumentContact object
 *
 * Get the list of addresses for this contact document.
 *
 * Returns: (element-type Couchdb.StructField) (transfer full): A #GSList of
 * #CouchdbStructField's representing all the addresses stored for this
 * contact.
 */
GSList *
couchdb_document_contact_get_addresses (CouchdbDocumentContact *document)
{
	GSList *list = NULL;
	JsonObject *addresses;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	if (json_object_has_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				    "addresses")) {
		addresses = json_object_get_object_member (
			couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)), "addresses");
		if (addresses) {
			json_object_foreach_member (addresses,
						    (JsonObjectForeach) foreach_object_cb,
						    &list);
		}
	}

	return list;
}

void
couchdb_document_contact_set_addresses (CouchdbDocumentContact *document, GSList *list)
{
	GSList *l;
	JsonObject *addresses;

	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));

	addresses = json_object_new ();
	for (l = list; l != NULL; l = l->next) {
		const gchar *street_str;
		CouchdbStructField *sf = (CouchdbStructField *) l->data;

		street_str = couchdb_document_contact_address_get_street (sf);
		if (street_str) {
			JsonObject *this_address;

			this_address = json_object_new ();

			json_object_set_string_member (this_address, "address1", street_str);
			json_object_set_string_member (this_address, "address2",
						       couchdb_document_contact_address_get_ext_street (sf));
			json_object_set_string_member (this_address, "city",
						       couchdb_document_contact_address_get_city (sf));
			json_object_set_string_member (this_address, "state",
						       couchdb_document_contact_address_get_state (sf));
			json_object_set_string_member (this_address, "country",
						       couchdb_document_contact_address_get_country (sf));
			json_object_set_string_member (this_address, "postalcode",
						       couchdb_document_contact_address_get_postalcode (sf));
			json_object_set_string_member (this_address, "pobox",
						       couchdb_document_contact_address_get_pobox (sf));
			json_object_set_string_member (this_address, "description",
						       couchdb_document_contact_address_get_description (sf));

			json_object_set_object_member (addresses,
						       couchdb_struct_field_get_uuid (sf),
						       this_address);
		}
	}

	json_object_set_object_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				       "addresses", addresses);
}

/**
 * couchdb_document_contact_get_im_addresses:
 * @document: A #CouchdbDocumentContact object
 *
 * Get the list of IM addresses for this contact document.
 *
 * Returns: (element-type Couchdb.StructField) (transfer full): A #GSList of
 * #CouchdbStructField's representing all the IM addresses stored for this
 * contact.
 */
GSList *
couchdb_document_contact_get_im_addresses (CouchdbDocumentContact *document)
{
	GSList *list = NULL;
	JsonObject *im_addresses;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	if (json_object_has_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				    "im_addresses")) {
		im_addresses = json_object_get_object_member (
			couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)), "im_addresses");
		if (im_addresses != NULL) {
			json_object_foreach_member (im_addresses,
						    (JsonObjectForeach) foreach_object_cb,
						    &list);
		}
	}

	return list;
}

void
couchdb_document_contact_set_im_addresses (CouchdbDocumentContact *document, GSList *list)
{
	GSList *l;
	JsonObject *im_addresses_json;

	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));

	im_addresses_json = json_object_new ();
	for (l = list; l != NULL; l = l->next) {
		const gchar *address_str;
		CouchdbStructField *sf = (CouchdbStructField *) l->data;

		address_str = couchdb_document_contact_im_get_address (sf);
		if (address_str != NULL) {
			JsonObject *this_address;

			this_address = json_object_new ();
			json_object_set_string_member (this_address, "address", address_str);
			json_object_set_string_member (this_address, "description",
						       couchdb_document_contact_im_get_description (sf));
			json_object_set_string_member (this_address, "protocol",
						       couchdb_document_contact_im_get_protocol (sf));

			json_object_set_object_member (im_addresses_json,
						       couchdb_struct_field_get_uuid (sf),
						       this_address);
		}
	}

	json_object_set_object_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				       "im_addresses", im_addresses_json);
}

/**
 * couchdb_document_contact_get_urls:
 * @document: A #CouchdbDocumentContact object
 *
 * Get the list of URLs for this contact document.
 *
 * Returns: (element-type Couchdb.StructField) (transfer full): A #GSList of
 * #CouchdbStructField's representing all the URLs stored for this
 * contact.
 */
GSList *
couchdb_document_contact_get_urls (CouchdbDocumentContact *document)
{
	GSList *list = NULL;
	JsonObject *urls;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	if (json_object_has_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				    "urls")) {
		urls = json_object_get_object_member (
			couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)), "urls");
		if (urls) {
			json_object_foreach_member (urls,
						    (JsonObjectForeach) foreach_object_cb,
						    &list);
		}
	}

	return list;
}

void
couchdb_document_contact_set_urls (CouchdbDocumentContact *document, GSList *list)
{
	GSList *l;
	JsonObject *urls_json;

	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));

	urls_json = json_object_new ();
	for (l = list; l != NULL; l = l->next) {
		const gchar *address_str;
		CouchdbStructField *sf = (CouchdbStructField *) l->data;

		address_str = couchdb_document_contact_url_get_address (sf);
		if (address_str) {
			JsonObject *this_url;

			this_url = json_object_new ();
			json_object_set_string_member (this_url, "address", address_str);
			json_object_set_string_member (this_url, "description",
						       couchdb_document_contact_url_get_description (sf));

			json_object_set_object_member (urls_json,
						       couchdb_struct_field_get_uuid (sf),
						       this_url);
		}
	}

	json_object_set_object_member (couchdb_document_get_json_object (COUCHDB_DOCUMENT (document)),
				       "urls", urls_json);
}

/**
 * couchdb_document_contact_get_categories:
 * @document: A #CouchdbDocumentContact object
 *
 * Get the list of categories (as a string) for this contact document.
 *
 * Returns: A comma separated list of categories as a string.
 */
const char *
couchdb_document_contact_get_categories (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "categories");
}

void
couchdb_document_contact_set_categories (CouchdbDocumentContact *document, const char *categories)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (categories != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "categories", categories);
}

const char *
couchdb_document_contact_get_notes (CouchdbDocumentContact *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document), NULL);
	g_return_val_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "notes");
}

void
couchdb_document_contact_set_notes (CouchdbDocumentContact *document, const char *notes)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT_CONTACT (document));
	g_return_if_fail (couchdb_document_is_contact (COUCHDB_DOCUMENT (document)));
	g_return_if_fail (notes != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "notes", notes);
}

/**
 * couchdb_document_contact_email_new:
 * @uuid: A unique ID
 * @address: Email address
 * @description: Description for this email address
 *
 * Returns a #CouchdbStructField object representing an email address:
 *
 * Returns: (transfer full): A newly-created #CouchdbStructField representing an
 * email address.
 */
CouchdbStructField *
couchdb_document_contact_email_new (const char *uuid, const char *address, const char *description)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new_from_json_object (json_object_new ());
	if (uuid != NULL)
		couchdb_struct_field_set_uuid (sf, uuid);
	else {
		char *new_uuid = generate_uuid ();
		couchdb_struct_field_set_uuid (sf, new_uuid);
		g_free (new_uuid);
	}

	if (address)
		couchdb_document_contact_email_set_address (sf, address);
	if (description)
		couchdb_document_contact_email_set_description (sf, description);

	return sf;
}

const char *
couchdb_document_contact_email_get_address (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "address");
}

void
couchdb_document_contact_email_set_address (CouchdbStructField *sf, const char *email)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (email != NULL);

	couchdb_struct_field_set_string_field (sf, "address", email);
}

const char *
couchdb_document_contact_email_get_description (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "description");
}

void
couchdb_document_contact_email_set_description (CouchdbStructField *sf, const char *description)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "description", description);
}

/**
 * couchdb_document_contact_phone_new:
 * @uuid: A unique ID
 * @number: A phone number
 * @description: Description for this phone number
 * @priority: Priority of this phone number
 *
 * Returns a #CouchdbStructField representing the given phone number.
 *
 * Returns: (transfer full): A newly-created #CouchdbStructField representing
 * the given phone number information.
 */
CouchdbStructField *
couchdb_document_contact_phone_new (const char *uuid, const char *number, const char *description, gint priority)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new_from_json_object (json_object_new ());
	if (uuid != NULL)
		couchdb_struct_field_set_uuid (sf, uuid);
	else {
		char *new_uuid = generate_uuid ();
		couchdb_struct_field_set_uuid (sf, new_uuid);
		g_free (new_uuid);
	}

	if (number)
		couchdb_document_contact_phone_set_number (sf, number);
	if (description)
		couchdb_document_contact_phone_set_description (sf, description);
	couchdb_document_contact_phone_set_priority (sf, priority);

	return sf;
}

gint
couchdb_document_contact_phone_get_priority (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, 0);

	return couchdb_struct_field_get_int_field (sf, "priority");
}

void
couchdb_document_contact_phone_set_priority (CouchdbStructField *sf, gint priority)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_int_field (sf, "priority", priority);
}

const char *
couchdb_document_contact_phone_get_number (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "number");
}

void
couchdb_document_contact_phone_set_number (CouchdbStructField *sf, const char *number)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (number != NULL);

	couchdb_struct_field_set_string_field (sf, "number", number);
}

const char *
couchdb_document_contact_phone_get_description (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "description");
}

void
couchdb_document_contact_phone_set_description (CouchdbStructField *sf, const char *description)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "description", description);
}

/**
 * couchdb_document_contact_address_new:
 * @uuid: A unique ID
 * @street: Street
 * @ext_street: Extra information for the street
 * @city: City
 * @state: State or region
 * @country: Country
 * @postalcode: Postal code
 * @pobox: Post Office box
 * @description: Description for thos address
 *
 * Returns a #CouchdbStructField representing the given address.
 *
 * Returns: (transfer full): A newly-created #CouchdbStructField representing
 * the given address information.
 */
CouchdbStructField *
couchdb_document_contact_address_new (const char *uuid,
				      const char *street,
				      const char *ext_street,
				      const char *city,
				      const char *state,
				      const char *country,
				      const char *postalcode,
				      const char *pobox,
				      const char *description)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new_from_json_object (json_object_new ());
	if (uuid != NULL)
		couchdb_struct_field_set_uuid (sf, uuid);
	else {
		char *new_uuid = generate_uuid ();
		couchdb_struct_field_set_uuid (sf, new_uuid);
		g_free (new_uuid);
	}

	if (street)
		couchdb_document_contact_address_set_street (sf, street);
	if (ext_street)
		couchdb_document_contact_address_set_ext_street (sf, ext_street);
	if (city)
		couchdb_document_contact_address_set_city (sf, city);
	if (state)
		couchdb_document_contact_address_set_state (sf, state);
	if (country)
		couchdb_document_contact_address_set_country (sf, country);
	if (postalcode)
		couchdb_document_contact_address_set_postalcode (sf, postalcode);
	if (pobox)
		couchdb_document_contact_address_set_pobox (sf, pobox);
	if (description)
		couchdb_document_contact_address_set_description (sf, description);

	return sf;
}

const char *
couchdb_document_contact_address_get_street (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	if (couchdb_struct_field_has_field (sf, "address1"))
		return couchdb_struct_field_get_string_field (sf, "address1");
	else if (couchdb_struct_field_has_field (sf, "street"))
		return couchdb_struct_field_get_string_field (sf, "street");

	return NULL;
}

void
couchdb_document_contact_address_set_street (CouchdbStructField *sf, const char *street)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "address1", street);
}

const char *
couchdb_document_contact_address_get_ext_street (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "address2");
}

void
couchdb_document_contact_address_set_ext_street (CouchdbStructField *sf, const char *ext_street)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "address2", ext_street);
}

const char *
couchdb_document_contact_address_get_city (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "city");
}

void
couchdb_document_contact_address_set_city (CouchdbStructField *sf, const char *city)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "city", city);
}

const char *
couchdb_document_contact_address_get_state (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "state");
}

void
couchdb_document_contact_address_set_state (CouchdbStructField *sf, const char *state)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "state", state);
}

const char *
couchdb_document_contact_address_get_country (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "country");
}

void
couchdb_document_contact_address_set_country (CouchdbStructField *sf, const char *country)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "country", country);
}

const char *
couchdb_document_contact_address_get_postalcode (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "postalcode");
}

void
couchdb_document_contact_address_set_postalcode (CouchdbStructField *sf, const char *postalcode)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "postalcode", postalcode);
}

const char *
couchdb_document_contact_address_get_pobox (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "pobox");
}

void
couchdb_document_contact_address_set_pobox (CouchdbStructField *sf, const char *pobox)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "pobox", pobox);
}

const char *
couchdb_document_contact_address_get_description (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "description");
}

void
couchdb_document_contact_address_set_description (CouchdbStructField *sf, const char *description)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "description", description);
}

/**
 * couchdb_document_contact_im_new:
 * @uuid: A unique ID
 * @address: IM address
 * @description: Description for this IM address
 * @protocol: Protocol used for this IM address
 *
 * Returns a #CouchdbStructField representing the given IM address.
 *
 * Returns: (transfer full): A newly-created #CouchdbStructField representing
 * the given IM address information.
 */
CouchdbStructField *
couchdb_document_contact_im_new (const char *uuid,
				 const char *address,
				 const char *description,
				 const char *protocol)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new_from_json_object (json_object_new ());
	if (uuid != NULL)
		couchdb_struct_field_set_uuid (sf, uuid);
	else {
		char *new_uuid = generate_uuid ();
		couchdb_struct_field_set_uuid (sf, new_uuid);
		g_free (new_uuid);
	}

	if (address != NULL)
		couchdb_document_contact_im_set_address (sf, address);
	if (description != NULL)
		couchdb_document_contact_im_set_description (sf, description);
	if (protocol != NULL)
		couchdb_document_contact_im_set_protocol (sf, protocol);

	return sf;
}

const char *
couchdb_document_contact_im_get_address (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "address");
}

void
couchdb_document_contact_im_set_address (CouchdbStructField *sf, const char *address)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (address != NULL);

	couchdb_struct_field_set_string_field (sf, "address", address);
}

const char *
couchdb_document_contact_im_get_description (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "description");
}

void
couchdb_document_contact_im_set_description (CouchdbStructField *sf, const char *description)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (description != NULL);

	couchdb_struct_field_set_string_field (sf, "description", description);
}

const char *
couchdb_document_contact_im_get_protocol (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "protocol");
}

void
couchdb_document_contact_im_set_protocol (CouchdbStructField *sf, const char *protocol)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (protocol != NULL);

	couchdb_struct_field_set_string_field (sf, "protocol", protocol);
}

/**
 * couchdb_document_contact_url_new:
 * @uuid: A unique ID
 * @address: URL address
 * @description: A description for this URL
 *
 * Returns a #CouchdbStructField representing the given URL.
 *
 * Returns: (transfer full): A newly-created #CouchdbStructField representing
 * the given URL information.
 */
CouchdbStructField *
couchdb_document_contact_url_new (const char *uuid, const char *address, const char *description)
{
	CouchdbStructField *sf;

	sf = couchdb_struct_field_new_from_json_object (json_object_new ());
	if (uuid != NULL)
		couchdb_struct_field_set_uuid (sf, uuid);
	else {
		char *new_uuid = generate_uuid ();
		couchdb_struct_field_set_uuid (sf, new_uuid);
		g_free (new_uuid);
	}

	if (address)
		couchdb_document_contact_url_set_address (sf, address);
	if (description)
		couchdb_document_contact_url_set_description (sf, description);

	return sf;
}

const char *
couchdb_document_contact_url_get_address (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "address");
}

void
couchdb_document_contact_url_set_address (CouchdbStructField *sf, const char *url)
{
	g_return_if_fail (sf != NULL);
	g_return_if_fail (url != NULL);

	couchdb_struct_field_set_string_field (sf, "address", url);
}

const char *
couchdb_document_contact_url_get_description (CouchdbStructField *sf)
{
	g_return_val_if_fail (sf != NULL, NULL);

	return couchdb_struct_field_get_string_field (sf, "description");
}

void
couchdb_document_contact_url_set_description (CouchdbStructField *sf, const char *description)
{
	g_return_if_fail (sf != NULL);

	couchdb_struct_field_set_string_field (sf, "description", description);
}
