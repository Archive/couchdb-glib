/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <libsoup/soup-message.h>
#include <libsoup/soup-uri.h>
#include "couchdb-glib.h"
#include "dbwatch.h"
#include "utils.h"

struct _CouchdbDatabasePrivate {
	CouchdbSession *session;
	char *dbname;
	DBWatch *db_watch;
};

enum {
	DOCUMENT_CREATED,
	DOCUMENT_UPDATED,
	DOCUMENT_DELETED,
	LAST_SIGNAL
};
static guint couchdb_database_signals[LAST_SIGNAL];

enum {
    PROP_0,
    PROP_SESSION,
    PROP_DATABASE_NAME
};

G_DEFINE_TYPE(CouchdbDatabase, couchdb_database, G_TYPE_OBJECT)

static void
couchdb_database_finalize (GObject *object)
{
	CouchdbDatabase *database = COUCHDB_DATABASE (object);

	if (database->priv->session != NULL) {
		g_object_unref (G_OBJECT (database->priv->session));
		database->priv->session = NULL;
	}

	if (database->priv->dbname != NULL) {
		g_free (database->priv->dbname);
		database->priv->dbname = NULL;
	}

	if (database->priv->db_watch != NULL) {
		dbwatch_free (database->priv->db_watch);
		database->priv->db_watch = NULL;
	}

	g_free (database->priv);

	G_OBJECT_CLASS (couchdb_database_parent_class)->finalize (object);
}

static void
couchdb_database_set_property (GObject *object,
			       guint prop_id,
			       const GValue *value,
			       GParamSpec *pspec)
{
	CouchdbDatabase *database = COUCHDB_DATABASE (object);

	switch (prop_id) {
	case PROP_SESSION:
		if (database->priv->session != NULL)
			g_object_unref (G_OBJECT (database->priv->session));
		database->priv->session = g_object_ref (G_OBJECT (g_value_get_object (value)));
		break;
	case PROP_DATABASE_NAME:
		g_free (database->priv->dbname);
		database->priv->dbname = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
couchdb_database_get_property (GObject *object,
			       guint prop_id,
			       GValue *value,
			       GParamSpec *pspec)
{
	CouchdbDatabase *database = COUCHDB_DATABASE (object);

	switch (prop_id) {
	case PROP_SESSION:
		g_value_set_object (value, database->priv->session);
		break;
	case PROP_DATABASE_NAME:
		g_value_set_string (value, database->priv->dbname);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
couchdb_database_class_init (CouchdbDatabaseClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = couchdb_database_finalize;
	object_class->set_property = couchdb_database_set_property;
	object_class->get_property = couchdb_database_get_property;

	g_object_class_install_property (object_class,
        	                         PROP_SESSION,
                	                 g_param_spec_object ("session",
                        	                              "Session",
							      "CouchDB session associated with this database",
							      COUCHDB_TYPE_SESSION,
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class,
        	                         PROP_DATABASE_NAME,
                	                 g_param_spec_string ("database_name",
                        	                              "Database name",
							      "Name of the database",
							      NULL,
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	/* Signals */
	couchdb_database_signals[DOCUMENT_CREATED] =
		g_signal_new ("document_created",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (CouchdbDatabaseClass, document_created),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1,
			      COUCHDB_TYPE_DOCUMENT);
	couchdb_database_signals[DOCUMENT_UPDATED] =
		g_signal_new ("document_updated",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (CouchdbDatabaseClass, document_updated),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1,
			      COUCHDB_TYPE_DOCUMENT);
	couchdb_database_signals[DOCUMENT_DELETED] =
		g_signal_new ("document_deleted",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (CouchdbDatabaseClass, document_deleted),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__STRING,
			      G_TYPE_NONE, 1,
			      G_TYPE_STRING);
}

static void
couchdb_database_init (CouchdbDatabase *database)
{
	database->priv = g_new0 (CouchdbDatabasePrivate, 1);
}

static CouchdbDocument *
create_document_from_json_object (JsonObject *json_object)
{
	CouchdbDocument *document = NULL;

	if (json_object_has_member (json_object, "_id") &&
	    g_str_has_prefix (json_object_get_string_member (json_object, "_id"), "_design/")) {
		document = COUCHDB_DOCUMENT (couchdb_design_document_new ());
		couchdb_document_set_from_json_object (document, json_object);
	} else {
		if (json_object_has_member (json_object, "record_type")) {
			if (g_strcmp0 (json_object_get_string_member (json_object, "record_type"),
				       COUCHDB_RECORD_TYPE_CONTACT) == 0) {
				document = COUCHDB_DOCUMENT (couchdb_document_contact_new ());
			} else if (g_strcmp0 (json_object_get_string_member (json_object, "record_type"),
					      COUCHDB_RECORD_TYPE_TASK) == 0) {
				document = COUCHDB_DOCUMENT (couchdb_document_task_new ());
			} else
				document = couchdb_document_new ();

			couchdb_document_set_from_json_object (document, json_object);
		} else
			document = couchdb_document_new_from_json_object (json_object);
	}

	return document;
}

/**
 * couchdb_database_new:
 * @session: A #CouchdbSession object
 * @dbname: Name of the database
 *
 * Create a new #CouchdbDatabase object, which is to be used for operations on specific
 * databases on the underlying CouchDB instance.
 *
 * Return value: A new #CouchdbDatabase object.
 */
CouchdbDatabase *
couchdb_database_new (CouchdbSession *session, const char *dbname)
{
	return g_object_new (COUCHDB_TYPE_DATABASE,
			     "session", session,
			     "database_name", dbname,
			     NULL);
}

static JsonArray *
call_all_docs_uri (CouchdbSession *session, const char *url, JsonParser *parser, GError **error)
{
	JsonArray *rows = NULL;

	if (couchdb_session_send_message (session, SOUP_METHOD_GET, url, NULL, parser, error)) {
		JsonNode *root_node;

		root_node = json_parser_get_root (parser);
		if (json_node_get_node_type (root_node) == JSON_NODE_OBJECT) {
			rows = json_object_get_array_member (
				json_node_get_object (root_node), "rows");
		}
	}

	return rows;
}

#ifdef COUCHDB_ENABLE_DEPRECATED

/**
 * couchdb_database_list_documents:
 * @database: A #CouchdbDatabase object
 * @error: Placeholder for error information
 *
 * Retrieve the list of all documents from a database on a running CouchDB instance.
 * For each document, a #CouchdbDocumentInfo object is returned on the list, which
 * can then be used for retrieving specific information for each document.
 *
 * Return Value: a list of #CouchdbDocumentInfo objects, or NULL if there are none
 * or there was an error (in which case the error argument will contain information
 * about the error). Once no longer needed, the list should be freed by calling
 * #couchdb_database_free_document_list.
 */
GSList *
couchdb_database_list_documents (CouchdbDatabase *database, GError **error)
{
	char *url;
	JsonParser *parser;
	JsonArray *rows;
	GSList *doclist = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);

	url = g_strdup_printf ("%s/%s/_all_docs",
			       couchdb_session_get_uri (database->priv->session),
			       database->priv->dbname);
	parser = json_parser_new ();

	rows = call_all_docs_uri (database->priv->session, url, parser, error);
	if (rows != NULL) {
		gint i;
		for (i = 0; i < json_array_get_length (rows); i++) {
			JsonObject *doc;
			CouchdbDocumentInfo *doc_info;

			doc = json_array_get_object_element (rows, i);
			if (!doc)
				continue;

			doc_info = couchdb_document_info_new (
				json_object_get_string_member (doc, "id"),
				json_object_get_string_member (
					json_object_get_object_member (doc, "value"),
					"rev"));
			if (!doc_info)
				continue;

			doclist = g_slist_append (doclist, doc_info);
		}
	}

	g_object_unref (G_OBJECT (parser));
	g_free (url);

	return doclist;
}
#endif 

/**
 * couchdb_database_get_design_documents:
 * @database: A #CouchdbDatabase object
 * @error: Placeholder for error information
 *
 * Retrieve all design documents from the given database.
 *
 * Design documents are special documents (well, they are really normal documents in
 * the CouchDB database, just with a special ID) that contain views' code, which are used
 * to create queries on the database that are cached and so make access to the database
 * much quicker.
 *
 * Returns: (element-type Couchdb.DesignDocument) (transfer full): A list of #CouchdbDesignDocument objects, or NULL if there are none
 * or there was an error (in which case the error argument will contain information
 * about the error). Once no longer needed, the list should be freed by calling
 * #couchdb_database_free_document_list.
 */
GSList *
couchdb_database_get_design_documents (CouchdbDatabase *database, GError **error)
{
	char *url;
	JsonParser *parser;
	JsonArray *rows;
	GSList *doclist = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);

	url = g_strdup_printf ("%s/%s/_all_docs?include_docs=true",
			       couchdb_session_get_uri (database->priv->session),
			       database->priv->dbname);
	parser = json_parser_new ();

	rows = call_all_docs_uri (database->priv->session, url, parser, error);
	if (rows != NULL) {
		gint i;
		for (i = 0; i < json_array_get_length (rows); i++) {
			JsonObject *obj, *doc;
			const gchar *id;
			CouchdbDesignDocument *document;

			obj = json_array_get_object_element (rows, i);
			if (!obj)
				continue;

			doc = json_object_get_object_member (obj, "doc");
			if (!doc)
				continue;

			id = json_object_get_string_member (doc, "_id");
			if (!g_str_has_prefix (id, "_design/"))
				continue;

			document = couchdb_design_document_new ();
			if (document != NULL) {
				couchdb_document_set_from_json_object (COUCHDB_DOCUMENT (document), doc);
				doclist = g_slist_append (doclist, document);
			}
		}
	}

	g_object_unref (G_OBJECT (parser));
	g_free (url);

	return doclist;
}

/**
 * couchdb_database_get_all_documents:
 * @database: A #CouchdbDatabase object
 * @error: Placeholder for error information
 *
 * Retrieve all documents from a database on a running CouchDB instance. For
 * each document found in the database, a #CouchdbDocument object is returned
 * on the list, which represents the document's contents as found on the
 * underlying database.
 *
 * Returns: (element-type Couchdb.Document) (transfer full): a list of #CouchdbDocument objects, or NULL if there are none
 * or there was an error (in which case the error argument will contain information
 * about the error). Once no longer needed, the list should be freed by calling
 * #couchdb_database_free_document_list.
 */
GSList *
couchdb_database_get_all_documents (CouchdbDatabase *database, GError **error)
{
	char *url;
	JsonParser *parser;
	JsonArray *rows;
	GSList *doclist = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);

	url = g_strdup_printf ("%s/%s/_all_docs?include_docs=true",
			       couchdb_session_get_uri (database->priv->session),
			       database->priv->dbname);
	parser = json_parser_new ();

	rows = call_all_docs_uri (database->priv->session, url, parser, error);
	if (rows != NULL) {
		gint i;
		for (i = 0; i < json_array_get_length (rows); i++) {
			JsonObject *obj;
			CouchdbDocument *document = NULL;

			obj = json_array_get_object_element (rows, i);
			if (!obj)
				continue;

			if (json_object_has_member (obj, "doc")) {
				document = create_document_from_json_object (json_object_get_object_member (obj, "doc"));
				if (document != NULL) {
					g_object_set (G_OBJECT (document), "database", database, NULL);
					doclist = g_slist_append (doclist, document);
				}
			}
		}
	}

	g_object_unref (G_OBJECT (parser));
	g_free (url);

	return doclist;
}

/**
 * couchdb_database_execute_view:
 * @database: A #CouchdbDatabase object
 * @design_doc: Name of the design document where the view to execute is
 * @view_name: Name of the view to execute
 * @error: Placeholder for error information
 *
 * Run a view on the database to retrieve documents. For
 * each document found in the database, a #CouchdbDocument object is returned
 * on the list, which represents the document's contents as found on the
 * underlying database.
 *
 * Returns: (element-type Couchdb.Document) (transfer full): a list of #CouchdbDocument objects, or NULL if there are none
 * or there was an error (in which case the error argument will contain information
 * about the error). Once no longer needed, the list should be freed by calling
 * #couchdb_database_free_document_list.
 */
GSList *
couchdb_database_execute_view (CouchdbDatabase *database,
			       const char *design_doc,
			       const char *view_name,
			       GError **error)
{
	char *url;
	JsonParser *parser;
	JsonArray *rows;
	GSList *doclist = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);
	g_return_val_if_fail (design_doc != NULL, NULL);
	g_return_val_if_fail (view_name != NULL, NULL);

	if (g_str_has_prefix (design_doc, "_design/")) {
		url = g_strdup_printf ("%s/%s/%s/_view/%s",
				       couchdb_session_get_uri (database->priv->session),
				       database->priv->dbname,
				       design_doc,
				       view_name);
	} else {
		url = g_strdup_printf ("%s/%s/_design/%s/_view/%s",
				       couchdb_session_get_uri (database->priv->session),
				       database->priv->dbname,
				       design_doc,
				       view_name);
	}

	parser = json_parser_new ();

	rows = call_all_docs_uri (database->priv->session, url, parser, error);
	if (rows != NULL) {
		gint i;
		for (i = 0; i < json_array_get_length (rows); i++) {
			JsonObject *obj;
			CouchdbDocument *document = NULL;

			obj = json_array_get_object_element (rows, i);
			if (!obj)
				continue;

			document = create_document_from_json_object (json_object_get_object_member (obj, "value"));

			if (document != NULL) {
				couchdb_document_set_id (document, json_object_get_string_member (obj, "id"));
				g_object_set (G_OBJECT (document), "database", database, NULL);
				doclist = g_slist_append (doclist, document);
			}
		}
	}

	g_object_unref (G_OBJECT (parser));
	g_free (url);

	return doclist;
}

/**
 * couchdb_session_free_document_list:
 * @doclist: A list of #CouchdbDocumentInfo or #CouchdbDocument objects, as returned by
 * #couchdb_database_list_documents or #couchdb_database_get_all_documents
 *
 * Free the list of documents returned by #couchdb_database_list_documents or
 * #couchdb_database_get_all_documents
 */
void
couchdb_database_free_document_list (GSList *doclist)
{
	g_return_if_fail (doclist != NULL);

	while (doclist != NULL) {
		gpointer data = doclist->data;

		doclist = g_slist_remove (doclist, data);

		g_object_unref (G_OBJECT (data));
	}
}

/**
 * couchdb_database_get_document:
 * @database: A #CouchdbDatabase object
 * @docid: Unique ID of the document to be retrieved
 * @error: Placeholder for error information
 *
 * Retrieve the last revision of a document from the given database.
 *
 * Returns: (transfer full): A #CouchdbDocument object if successful, NULL otherwise, in
 * which case, the error argument will contain information about the error.
 */
CouchdbDocument *
couchdb_database_get_document (CouchdbDatabase *database,
			       const char *docid,
			       GError **error)
{
	char *url, *encoded_docid;
	JsonParser *parser;
	CouchdbDocument *document = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);
	g_return_val_if_fail (docid != NULL, NULL);

	encoded_docid = soup_uri_encode (docid, NULL);
	url = g_strdup_printf ("%s/%s/%s", couchdb_session_get_uri (database->priv->session),
			       database->priv->dbname, encoded_docid);
	parser = json_parser_new ();
	if (couchdb_session_send_message (database->priv->session, SOUP_METHOD_GET, url, NULL, parser, error)) {
		JsonObject *json_object = json_node_get_object (json_parser_get_root (parser));

		document = create_document_from_json_object (json_object);
		g_object_set (G_OBJECT (document), "database", database, NULL);
	}

	g_object_unref (G_OBJECT (parser));
	g_free (encoded_docid);
	g_free (url);

	return document;
}

/**
 * couchdb_database_get_design_document:
 * @database: A #CouchdbDatabase object
 * @docid: ID of the design document
 * @error: Placeholder for error information
 *
 * Retrieve a design document from the given database.
 *
 * Returns: (transfer full): A #CouchdbDesignDocument object if successful, NULL otherwise, in
 * which case, the error argument will contain information about the error.
 */
CouchdbDesignDocument *
couchdb_database_get_design_document (CouchdbDatabase *database, const char *docid, GError **error)
{
	char *url;
	JsonParser *parser;
	CouchdbDesignDocument *document = NULL;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);
	g_return_val_if_fail (docid != NULL, NULL);

	if (g_str_has_prefix (docid, "_design/")) {
		url = g_strdup_printf ("%s/%s/%s", couchdb_session_get_uri (database->priv->session),
				       database->priv->dbname, docid);
	} else {
		url = g_strdup_printf ("%s/%s/_design/%s", couchdb_session_get_uri (database->priv->session),
				       database->priv->dbname, docid);
	}

	parser = json_parser_new ();
	if (couchdb_session_send_message (database->priv->session, SOUP_METHOD_GET, url, NULL, parser, error)) {
		document = couchdb_design_document_new ();
		couchdb_document_set_from_json_object (COUCHDB_DOCUMENT (document),
						       json_node_get_object (json_parser_get_root (parser)));

		g_object_set (G_OBJECT (document), "database", database, NULL);
	}

	g_object_unref (G_OBJECT (parser));
	g_free (url);

	return document;
}

/**
 * couchdb_database_put_document:
 * @database: A #CouchdbDatabase object
 * @document: A #CouchdbDocument object
 * @error: Placeholder for error information
 *
 * Store a document on a CouchDB database.
 *
 * If it is a new document, and hence does not have a unique ID, a unique ID
 * will be generated and stored on the #CouchdbDocument object. Likewise,
 * whether the document is new or just an update to an existing one, the
 * #CouchdbDocument object passed to this function will be updated to contain
 * the latest revision of the document, as returned by CouchDB (revision that
 * can be retrieved by calling #couchdb_document_get_revision).
 *
 * Return value: TRUE if successful, FALSE otherwise, in which case the error
 * argument will contain information about the error.
 */
gboolean
couchdb_database_put_document (CouchdbDatabase *database,
			       CouchdbDocument *document,
			       GError **error)
{
	char *url, *body;
	const char *id;
	JsonParser *parser;
	gboolean result = FALSE;
	gboolean send_ok;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), FALSE);
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);

	id = couchdb_document_get_id (document);
	body = couchdb_document_to_string (document);
	parser = json_parser_new ();
	if (id) {
		char *encoded_docid, **special_name_parts;

		/* Allow one sub-level in the ID string for i. e. design documents */
		special_name_parts = *id == '_' ? g_strsplit (id, "/", 2) : NULL;
		if (special_name_parts == NULL) {
			encoded_docid = soup_uri_encode (id, NULL);
			url = g_strdup_printf ("%s/%s/%s", couchdb_session_get_uri (database->priv->session), database->priv->dbname, encoded_docid);
		}
		else {
			encoded_docid = soup_uri_encode (special_name_parts[1], NULL);
			url = g_strdup_printf ("%s/%s/%s/%s",
					       couchdb_session_get_uri (database->priv->session),
					       database->priv->dbname,
					       special_name_parts[0],
					       encoded_docid);
			g_strfreev (special_name_parts);
		}

		send_ok = couchdb_session_send_message (database->priv->session, SOUP_METHOD_PUT, url, body, parser, error);

		g_free (encoded_docid);
	} else {
		url = g_strdup_printf ("%s/%s/", couchdb_session_get_uri (database->priv->session), database->priv->dbname);
		send_ok = couchdb_session_send_message (database->priv->session, SOUP_METHOD_POST, url, body, parser, error);
	}

	if (send_ok) {
		JsonObject *object;

		object = json_node_get_object (json_parser_get_root (parser));
		couchdb_document_set_id (document, json_object_get_string_member (object, "id"));
		couchdb_document_set_revision (document, json_object_get_string_member (object, "rev"));

		if (id)
			g_signal_emit_by_name (database, "document_updated", document);
		else
			g_signal_emit_by_name (database, "document_created", document);

		g_object_set (G_OBJECT (document), "database", database, NULL);

		result = TRUE;
	}

	/* free memory */
	g_free (url);
	g_free (body);
	g_object_unref (G_OBJECT (parser));

	return result;
}

/**
 * couchdb_database_delete_document:
 * @database: A #CouchdbDatabase object
 * @document: A #CouchdbDocument object
 * @error: Placeholder for error information
 *
 * Delete an existing document from a CouchDB instance.
 *
 * Please take note that this operation can fail if there was an update to the
 * document and that last revision was not retrieved by the calling application.
 * This is due to the fact that, to remove a document from CouchDB, the latest
 * revision needs to be sent, so if the #CouchdbDocument object passed to this
 * function does not contain the last revision, the operation will fail. In that
 * case, retrieving the latest revision from CouchDB (with #couchdb_database_get_document)
 * and trying the delete operation again should fix the issue.
 *
 * Return value: TRUE if successful, FALSE otherwise, in which case the error
 * argument will contain information about the error.
 */
gboolean
couchdb_database_delete_document (CouchdbDatabase *database, CouchdbDocument *document, GError **error)
{
	const char *id, *revision;
	char *url;
	gboolean result = FALSE;

	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), FALSE);
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);

	id = couchdb_document_get_id (document);
	revision = couchdb_document_get_revision (document);
	if (!id || !revision) /* we can't remove a document without an ID and/or a REVISION */
		return FALSE;

	url = g_strdup_printf ("%s/%s/%s?rev=%s", couchdb_session_get_uri (database->priv->session), database->priv->dbname, id, revision);
	
	/* We don't parse the http response, therefore the parser arg is NULL */
	if (couchdb_session_send_message (database->priv->session, SOUP_METHOD_DELETE, url, NULL, NULL, error)) {
		result = TRUE;		
		g_signal_emit_by_name (database, "document_deleted", id);
	}

	g_free (url);

	return result;
}

/**
 * couchdb_database_listen_for_changes:
 * @database: A #CouchdbDatabase object
 *
 * Setup a listener to get information about changes done to a specific database. Please
 * note that changes done in the application using couchdb-glib will be notified
 * without the need of calling this function. But if the application wants to receive
 * notifications of changes done externally (by another application, or by any other
 * means, like replication with a remote database), it needs to call this function.
 *
 * For each change, one of the signals on the #CouchdbDatabase object will be emitted,
 * so applications just have to connect to those signals before calling this function.
 */
void
couchdb_database_listen_for_changes (CouchdbDatabase *database)
{
	CouchdbDatabaseInfo *db_info;
	GError *error = NULL;

	g_return_if_fail (COUCHDB_IS_DATABASE (database));

	if (database->priv->db_watch != NULL) {
		g_warning ("Already listening for changes in '%s' database", database->priv->dbname);
		return;
	}

	/* Retrieve information for database, to know the last_update_sequence */
	db_info = couchdb_session_get_database_info (database->priv->session,
						     database->priv->dbname,
						     &error);
	if (!db_info) {
		g_warning ("Could not retrieve information for '%s' database: %s",
			   database->priv->dbname, error->message);
		g_error_free (error);

		return;
	}

	database->priv->db_watch = dbwatch_new (database,
						couchdb_database_info_get_update_sequence (db_info));

	/* Free memory */
	couchdb_database_info_unref (db_info);
}

/**
 * couchdb_database_get_session:
 * @database: A #CouchdbDatabase object
 *
 * Retrieve the #CouchdbSession associated with the given database object.
 *
 * Returns: (transfer none): A #CouchdbSession object.
 */
CouchdbSession *
couchdb_database_get_session (CouchdbDatabase *database)
{
	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);

	return database->priv->session;
}

/**
 * couchdb_database_get_name:
 * @database: A #CouchdbDatabase object
 *
 * Retrieve the name of the database the given #CouchdbDatabase object maps to.
 *
 * Return value: The name of the database
 */
const char *
couchdb_database_get_name (CouchdbDatabase *database)
{
	g_return_val_if_fail (COUCHDB_IS_DATABASE (database), NULL);

	return database->priv->dbname;
}
