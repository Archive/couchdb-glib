/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Krzysztof Klimonda
 *
 * Authors: Krzysztof Klimonda <kklimonda@syntaxhighlighted.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef __COUCHDB_QUERY_H__
#define __COUCHDB_QUERY_H__

#include <glib.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS

#define COUCHDB_TYPE_QUERY (couchdb_query_get_type ())
#define COUCHDB_QUERY(obj)					\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj),			\
				     COUCHDB_TYPE_QUERY,	\
				     CouchdbQuery))
#define COUCHDB_IS_QUERY(obj)					\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj),			\
				     COUCHDB_TYPE_QUERY))
#define COUCHDB_QUERY_CLASS(klass)			\
	(G_TYPE_CHECK_CLASS_CAST ((klass),		\
				  COUCHDB_TYPE_QUERY,	\
				  CouchdbQueryClass))
#define COUCHDB_IS_QUERY_CLASS(klass)			\
	(G_TYPE_CHECK_CLASS_TYPE ((klass),		\
				  COUCHDB_TYPE_QUERY))
#define COUCHDB_QUERY_GET_CLASS(obj)			\
	(G_TYPE_INSTANCE_GET_CLASS ((obj),		\
				    COUCHDB_TYPE_QUERY, \
				    CouchdbQueryClass))

typedef struct _CouchdbQueryPrivate CouchdbQueryPrivate;

typedef struct {
	GObject parent;
	CouchdbQueryPrivate *priv;
} CouchdbQuery;

typedef struct {
	GObjectClass parent_class;
} CouchdbQueryClass;

GType         couchdb_query_get_type (void);
CouchdbQuery *couchdb_query_new (void);
CouchdbQuery *couchdb_query_new_for_path (const char *path);
CouchdbQuery *couchdb_query_new_for_view (const char * design_doc,
					  const char * view_name);

const char   *couchdb_query_get_path (CouchdbQuery * self);
void          couchdb_query_set_path (CouchdbQuery * self,
				      const char * path);
const char   *couchdb_query_get_method (CouchdbQuery * self);
void          couchdb_query_set_method (CouchdbQuery * self,
					const char * method);
const char   *couchdb_query_get_option (CouchdbQuery * self,
					const char * name);
void          couchdb_query_set_option (CouchdbQuery * self,
					const char * name,
					const char * value);

const char   *couchdb_query_get_query_options_string (CouchdbQuery * self);
JsonObject   *couchdb_query_get_json_object (CouchdbQuery * self);
void          couchdb_query_set_json_object (CouchdbQuery * self,
					     JsonObject * object);

G_END_DECLS

#endif /* __COUCHDB_QUERY_H__ */
