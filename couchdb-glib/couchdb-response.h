/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Krzysztof Klimonda
 *
 * Authors: Krzysztof Klimonda <kklimonda@syntaxhighlighted.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef __COUCHDB_RESPONSE_H__
#define __COUCHDB_RESPONSE_H__

#include <glib.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS

#define COUCHDB_TYPE_RESPONSE (couchdb_response_get_type ())
#define COUCHDB_RESPONSE(obj)					\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj),			\
				     COUCHDB_TYPE_RESPONSE,	\
				     CouchdbResponse))
#define COUCHDB_IS_RESPONSE(obj)				\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj),			\
				     COUCHDB_TYPE_RESPONSE))
#define COUCHDB_RESPONSE_CLASS(klass)				\
	(G_TYPE_CHECK_CLASS_CAST ((klass),			\
				  COUCHDB_TYPE_RESPONSE,	\
				  CouchdbResponseClass))
#define COUCHDB_IS_RESPONSE_CLASS(klass)			\
	(G_TYPE_CHECK_CLASS_TYPE ((klass),			\
				  COUCHDB_TYPE_RESPONSE))

typedef struct _CouchdbResponsePrivate CouchdbResponsePrivate;

typedef struct {
	GObject parent;
	CouchdbResponsePrivate *priv;
} CouchdbResponse;

typedef struct {
	GObjectClass parent_class;
} CouchdbResponseClass;

GType            couchdb_response_get_type (void);
CouchdbResponse *couchdb_response_new (void);

const char      *couchdb_response_get_etag (CouchdbResponse *self);
guint            couchdb_response_get_status_code (CouchdbResponse *self);
const char      *couchdb_response_get_content_type (CouchdbResponse *self);
gsize            couchdb_response_get_content_length (CouchdbResponse *self);
JsonObject      *couchdb_response_get_json_object (CouchdbResponse *self);
GList           *couchdb_response_get_rows (CouchdbResponse *self);

G_END_DECLS

#endif /* __COUCHDB_RESPONSE_H__ */
