/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "couchdb-design-document.h"
#include "utils.h"

G_DEFINE_TYPE(CouchdbDesignDocument, couchdb_design_document, COUCHDB_TYPE_DOCUMENT)

static void
couchdb_design_document_class_init (CouchdbDesignDocumentClass *klass)
{
}

static void
couchdb_design_document_init (CouchdbDesignDocument *document)
{
}

/**
 * couchdb_design_document_new:
 *
 * Create a new design document, to contain views' code.
 *
 * Return value: A new #CouchdbDesignDocument object.
 */
CouchdbDesignDocument *
couchdb_design_document_new (void)
{
	return g_object_new (COUCHDB_TYPE_DESIGN_DOCUMENT, NULL);
}

/**
 * couchdb_design_document_list_views:
 * @document: A #CouchdbDesignDocument object
 *
 * Return a list of the names of the views contained in the given
 * #CouchdbDesignDocument object.
 *
 * Returns: (element-type utf8) (transfer full): A list of names of views. When no longer needed, this list
 * should be freed by calling #couchdb_design_document_free_views_list.
 */
GSList *
couchdb_design_document_list_views (CouchdbDesignDocument *document)
{
	JsonObject *json_object;
	GSList *list = NULL;

	g_return_val_if_fail (COUCHDB_IS_DESIGN_DOCUMENT (document), NULL);

	json_object = couchdb_document_get_json_object (COUCHDB_DOCUMENT (document));
	if (json_object != NULL) {
		if (json_object_has_member (json_object, "views")) {
			GList *members_list, *l;

			members_list = json_object_get_members (
				json_object_get_object_member (json_object, "views"));
			for (l = members_list; l != NULL; l = l->next)
				list = g_slist_append (list, g_strdup (l->data));

			g_list_free (members_list);
		}
	}

	return list;
}

/**
 * couchdb_design_document_free_views_list:
 * @list: A list of views' names, as returned by #couchdb_design_document_list_views
 *
 * Free the list of views' names returned by #couchdb_design_document_list_views.
 */
void
couchdb_design_document_free_views_list (GSList *list)
{
	while (list != NULL) {
		char *name = list->data;

		list = g_slist_remove (list, name);
		g_free (name);
	}
}

/**
 * couchdb_design_document_add_view:
 * @document: A #CouchdbDesignDocument object
 * @name: Name of the view
 * @map_function: Source code for the map function
 * @reduce_function: Source code for the reduce function
 *
 * Add a new view to the given #CouchdbDesignDocument object. If the view already exists,
 * it will get replaced by the new one. To make the change permanent, the document
 * must be saved to the database by calling #couchdb_database_put_document.
 */
void
couchdb_design_document_add_view (CouchdbDesignDocument *document,
				  const char *name,
				  const char *map_function,
				  const char *reduce_function)
{
	JsonObject *json_object;

	g_return_if_fail (COUCHDB_IS_DESIGN_DOCUMENT (document));
	g_return_if_fail (name != NULL);

	json_object = couchdb_document_get_json_object (COUCHDB_DOCUMENT (document));
	if (json_object != NULL) {
		JsonObject *json_views, *this_view;

		if (json_object_has_member (json_object, "views"))
			json_views = json_object_get_object_member (json_object, "views");
		else {
			json_views = json_object_new ();
			json_object_set_object_member (json_object, "views", json_views);
		}

		this_view = json_object_new ();
		if (map_function != NULL)
			json_object_set_string_member (this_view, "map", map_function);
		if (reduce_function != NULL)
			json_object_set_string_member (this_view, "reduce", reduce_function);

		json_object_set_object_member (json_views, name, this_view);
	}
}

/**
 * couchdb_design_document_delete_view:
 * @document: A #CouchdbDesignDocument object
 * @name: Name of the view to remove
 *
 * Delete a view from the given #CouchdbDesignDocument object. To make the change permanent,
 * the document must be saved to the database by calling #couchdb_database_put_document.
 */
void
couchdb_design_document_delete_view (CouchdbDesignDocument *document,
				     const char *name)
{
	JsonObject *json_object;

	g_return_if_fail (COUCHDB_IS_DESIGN_DOCUMENT (document));
	g_return_if_fail (name != NULL);

	json_object = couchdb_document_get_json_object (COUCHDB_DOCUMENT (document));
	if (json_object != NULL) {
		JsonObject *json_views;

		json_views = json_object_get_object_member (json_object, "views");
		if (json_views != NULL && json_object_has_member (json_views, name))
			json_object_remove_member (json_views, name);
	}
}

/**
 * couchdb_design_document_get_language:
 * @document: A #CouchdbDesignDocument object
 *
 * Return the programming language in which the views on the given design
 * document are written.
 *
 * Return value: A value representing the language the views are written in.
 */
CouchdbDesignDocumentLanguage
couchdb_design_document_get_language (CouchdbDesignDocument *document)
{
	JsonObject *json_object;

	g_return_val_if_fail (COUCHDB_IS_DESIGN_DOCUMENT (document), COUCHDB_DESIGN_DOCUMENT_LANGUAGE_UNKNOWN);

	json_object = couchdb_document_get_json_object (COUCHDB_DOCUMENT (document));
	if (json_object != NULL) {
		const gchar *language;

		language = json_object_get_string_member (json_object, "language");
		if (g_strcmp0 (language, "javascript") == 0)
			return COUCHDB_DESIGN_DOCUMENT_LANGUAGE_JAVASCRIPT;
		else if (g_strcmp0 (language, "python") == 0)
			return COUCHDB_DESIGN_DOCUMENT_LANGUAGE_PYTHON;
	}

	return COUCHDB_DESIGN_DOCUMENT_LANGUAGE_UNKNOWN;
}

/**
 * couchdb_design_document_set_language:
 * @document: A #CouchdbDesignDocument object
 * @language: Language for the views in this design document
 *
 * Set the language used in the views contained in the given #CouchdbDesignDocument
 * object.
 */
void
couchdb_design_document_set_language (CouchdbDesignDocument *document,
				      CouchdbDesignDocumentLanguage language)
{
	JsonObject *json_object;

	g_return_if_fail (COUCHDB_IS_DESIGN_DOCUMENT (document));

	json_object = couchdb_document_get_json_object (COUCHDB_DOCUMENT (document));
	if (json_object != NULL) {
		switch (language) {
		case COUCHDB_DESIGN_DOCUMENT_LANGUAGE_JAVASCRIPT:
			json_object_set_string_member (json_object, "language", "javascript");
			break;
		case COUCHDB_DESIGN_DOCUMENT_LANGUAGE_PYTHON:
			json_object_set_string_member (json_object, "language", "python");
			break;
		default:
			break;
		}
	}
}
