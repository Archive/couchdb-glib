/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COUCHDB_DATABASE_H__
#define __COUCHDB_DATABASE_H__

#include <glib-object.h>
#include "couchdb-design-document.h"
#include "couchdb-document.h"
#include "couchdb-session.h"

G_BEGIN_DECLS

#define COUCHDB_TYPE_DATABASE                (couchdb_database_get_type ())
#define COUCHDB_DATABASE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), COUCHDB_TYPE_DATABASE, CouchdbDatabase))
#define COUCHDB_IS_DATABASE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), COUCHDB_TYPE_DATABASE))
#define COUCHDB_DATABASE_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), COUCHDB_TYPE_DATABASE, CouchdbDatabaseClass))
#define COUCHDB_IS_DATABASE_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), COUCHDB_TYPE_DATABASE))
#define COUCHDB_DATABASE_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), COUCHDB_TYPE_DATABASE, CouchdbDatabaseClass))

typedef struct _CouchdbDatabasePrivate CouchdbDatabasePrivate;

struct _CouchdbDatabase {
	GObject parent;
	CouchdbDatabasePrivate *priv;
};

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (* document_created) (CouchdbDatabase *database, CouchdbDocument *document);
	void (* document_updated) (CouchdbDatabase *database, CouchdbDocument *document);
	void (* document_deleted) (CouchdbDatabase *database, const char *docid);
} CouchdbDatabaseClass;

GType            couchdb_database_get_type (void);
CouchdbDatabase *couchdb_database_new (CouchdbSession *session, const char *dbname);

#ifdef COUCHDB_ENABLE_DEPRECATED
GSList          *couchdb_database_list_documents (CouchdbDatabase *database, GError **error);
#endif
GSList          *couchdb_database_get_design_documents (CouchdbDatabase *database, GError **error);
GSList          *couchdb_database_get_all_documents (CouchdbDatabase *database, GError **error);
GSList          *couchdb_database_execute_view (CouchdbDatabase *database,
						const char *design_doc,
						const char *view_name,
						GError **error);
void             couchdb_database_free_document_list (GSList *doclist);

CouchdbDocument *couchdb_database_get_document (CouchdbDatabase *database,
						const char *docid,
						GError **error);
CouchdbDesignDocument *couchdb_database_get_design_document (CouchdbDatabase *database,
							     const char *docid,
							     GError **error);
gboolean         couchdb_database_put_document (CouchdbDatabase *database,
						CouchdbDocument *document,
						GError **error);
gboolean         couchdb_database_delete_document (CouchdbDatabase *database, CouchdbDocument *document, GError **error);

void             couchdb_database_listen_for_changes (CouchdbDatabase *database);

CouchdbSession  *couchdb_database_get_session (CouchdbDatabase *database);
const char      *couchdb_database_get_name (CouchdbDatabase *database);

G_END_DECLS

#endif
