/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <uuid/uuid.h>
#include <string.h>
#include <libsoup/soup-session-async.h>
#include <libsoup/soup-uri.h>
#include "couchdb-glib.h"
#include "utils.h"

GQuark
couchdb_error_quark (void)
{
	static GQuark error;

	if (!error)
		error = g_quark_from_static_string ("couchdb_glib");

	return error;
}

char *
generate_uuid (void)
{
	uuid_t uuid;
	char uuid_string[37];

	uuid_generate_random (uuid);
	uuid_unparse (uuid, uuid_string);

	return g_strdup (uuid_string);
}

/* following characters have to be explicitly added
 * to soup_uri_encode call as it doesn't escape
 * sub-delims defined in RFC 3986, part 2.2. */
gchar *
couchdb_uri_encode (const gchar *uri)
{
        return soup_uri_encode (uri, "&;_$()+-");
}

GType
get_type_from_field (JsonObject *json_object, const gchar *field)
{
	JsonNode *node;
	GType type;

	node = json_object_get_member (json_object, field);
	if (node == NULL)
		return G_TYPE_INVALID;

	type = json_node_get_value_type (node);
	switch (type) {
	case JSON_NODE_OBJECT:
		return COUCHDB_TYPE_STRUCT_FIELD;
	case JSON_NODE_ARRAY:
		return COUCHDB_TYPE_ARRAY_FIELD;
	default:
		return type;
	}
}
