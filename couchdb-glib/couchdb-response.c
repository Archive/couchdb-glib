/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Krzysztof Klimonda
 *
 * Authors: Krzysztof Klimonda <kklimonda@syntaxhighlighted.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "couchdb-response.h"

#define GET_PRIVATE(obj)					\
	(G_TYPE_INSTANCE_GET_PRIVATE ((obj),			\
				      COUCHDB_TYPE_RESPONSE,	\
				      CouchdbResponsePrivate))

G_DEFINE_TYPE(CouchdbResponse, couchdb_response, G_TYPE_OBJECT)

struct _CouchdbResponsePrivate {
	JsonObject *response;
	gchar *etag;
	guint status_code;
	gchar *content_type;
	gsize content_length;
};

enum {
	PROP_0,
	PROP_RESPONSE,
	PROP_ETAG,
	PROP_STATUS_CODE,
	PROP_CONTENT_TYPE,
	PROP_CONTENT_LENGTH
};

static void
couchdb_response_get_property (GObject *object, guint property_id,
			       GValue *value, GParamSpec *pspec)
{
	CouchdbResponse *self;

	self = COUCHDB_RESPONSE (object);

	switch (property_id) {
 	case PROP_RESPONSE:
		g_value_set_boxed (value, self->priv->response);
		break;
	case PROP_ETAG:
		g_value_set_string (value, self->priv->etag);
		break;
	case PROP_STATUS_CODE:
		g_value_set_uint (value, self->priv->status_code);
		break;
	case PROP_CONTENT_TYPE:
		g_value_set_string (value, self->priv->content_type);
		break;
	case PROP_CONTENT_LENGTH:
		g_value_set_ulong (value, self->priv->content_length);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
couchdb_response_set_property (GObject *object, guint property_id,
			       const GValue *value, GParamSpec *pspec)
{
	CouchdbResponse *self;

	self = COUCHDB_RESPONSE (object);

	switch (property_id) {
	case PROP_RESPONSE:
		if (self->priv->response != NULL)
			json_object_unref (self->priv->response);
		self->priv->response = g_value_dup_boxed (value);
		break;
	case PROP_ETAG:
		if (self->priv->etag)
			g_free (self->priv->etag);
		self->priv->etag = g_value_dup_string (value);
		break;
	case PROP_STATUS_CODE:
		self->priv->status_code = g_value_get_uint (value);
		break;
	case PROP_CONTENT_TYPE:
		if (self->priv->content_type)
			g_free (self->priv->content_type);
		self->priv->content_type = g_value_dup_string (value);
		break;
	case PROP_CONTENT_LENGTH:
		self->priv->content_length = g_value_get_ulong (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
couchdb_response_finalize (GObject *object)
{
	CouchdbResponse *self = COUCHDB_RESPONSE (object);
	
	if (self->priv->response) {
		json_object_unref (self->priv->response);
		self->priv->response = NULL;
	}

	g_free (self->priv->etag);
	g_free (self->priv->content_type);

	G_OBJECT_CLASS (couchdb_response_parent_class)->finalize (object);
}

static void
couchdb_response_class_init (CouchdbResponseClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec;

	gobject_class->finalize = couchdb_response_finalize;
	gobject_class->set_property = couchdb_response_set_property;
	gobject_class->get_property = couchdb_response_get_property;

	g_type_class_add_private (klass, sizeof (CouchdbResponsePrivate));

	pspec = g_param_spec_boxed ("response", "Response",
				    "Json Response",
				    JSON_TYPE_OBJECT,
				    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (gobject_class, PROP_RESPONSE, pspec);

	pspec = g_param_spec_string ("etag", "ETag", "Entity Tag",
				     NULL,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (gobject_class, PROP_ETAG, pspec);

	pspec = g_param_spec_uint ("status-code", "Status Code",
				   "Http Response Status Code",
				   100, 599, 200,
				   G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (gobject_class, PROP_STATUS_CODE, pspec);

	pspec = g_param_spec_string ("content-type", "Content-Type",
				     "Response Content-Type",
				     NULL,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (gobject_class, PROP_CONTENT_TYPE, pspec);

	pspec = g_param_spec_ulong ("content-length", "Content Length",
				    "Content's Size",
				    0, G_MAXULONG, 0,
				    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (gobject_class, PROP_CONTENT_LENGTH, pspec);
}

static void
couchdb_response_init (CouchdbResponse *self)
{
	self->priv = GET_PRIVATE (self);

	self->priv->response = NULL;
}

/**
 * couchdb_response_get_json_object:
 * @self: A #CouchdbResponse object
 *
 * Returns a #JsonObject containing the response from CouchDB server.
 *
 * Return value: (transfer none): A #JsonObject containing response from
 * the server. Object is owned by #CouchdbResponse and should not be
 * freed.
 */
JsonObject *
couchdb_response_get_json_object(CouchdbResponse *self)
{
	g_return_val_if_fail(COUCHDB_IS_RESPONSE(self), NULL);

	return self->priv->response;
}

/**
 * couchdb_response_get_rows:
 * @self: A #CouchdbResponse object
 *
 * Returns a list of #JsonObject rows returned by the CouchDB server.
 *
 * Return value: (element-type Json.Object) (transfer container): A #GList
 * of #JsonObject objects. Returned objects are owned by #CouchdbResponse,
 * #GList should be freed by caller when no longer needed.
 */
GList *
couchdb_response_get_rows (CouchdbResponse *self)
{
	GList *nodes, *iter;

	nodes = json_array_get_elements (
		json_object_get_array_member (self->priv->response, "rows"));
	
	for (iter = nodes; iter != NULL; iter = g_list_next (iter)) {
		iter->data = json_node_get_object (iter->data);;
	}

	return nodes;
}

/**
 * couchdb_response_get_etag
 * @self: A #CouchdbResponse object
 *
 * Returns the value of ETag header from the response. This header
 * is set to the current revision of the document requested.
 *
 * Return value: A @string containing the ETag header or %NULL
 * if there has been no ETag in response headers.
 */
const gchar *
couchdb_response_get_etag (CouchdbResponse * self)
{
	g_return_val_if_fail (COUCHDB_IS_RESPONSE (self), NULL);

	return (const gchar *) self->priv->etag;
}

/**
 * couchdb_response_get_status_code
 * @self: A #CouchdbResponse object
 * 
 * Returns the response status code
 *
 * Return value: status code of the response
 */
guint
couchdb_response_get_status_code (CouchdbResponse * self)
{
	g_return_val_if_fail (COUCHDB_IS_RESPONSE (self), 500);

	return self->priv->status_code;
}

/**
 * couchdb_response_get_content_type
 * @self: A #CouchdbResponse object
 *
 * Returns the string containing content type of the response.
 * 
 * Return value: Content-Type of the response or %NULL if the header
 * wasn't set
 */
const gchar *
couchdb_response_get_content_type (CouchdbResponse * self)
{
	g_return_val_if_fail (COUCHDB_IS_RESPONSE (self), NULL);

	return (const gchar *) self->priv->content_type;
}

/**
 * couchdb_response_get_content_length
 * @self: A #CouchdbResponse object
 *
 * Returns content length of the CouchDB response.
 *
 * Return value: content length or 0 if response doesn't contain
 * any body.
 */
gsize
couchdb_response_get_content_length (CouchdbResponse * self)
{
	g_return_val_if_fail (COUCHDB_IS_RESPONSE (self), 0);

	return self->priv->content_length;
}
