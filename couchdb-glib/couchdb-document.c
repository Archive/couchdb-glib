/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009-2010 Canonical Services Ltd (www.canonical.com)
 *               2009 Mikkel Kamstrup Erlandsen
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *          Mikkel Kamstrup Erlandsen <mikkel.kamstrup@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <libsoup/soup-logger.h>
#include <libsoup/soup-gnome.h>
#include <json-glib/json-glib.h>
#include "couchdb-database.h"
#include "couchdb-document.h"
#include "utils.h"

struct _CouchdbDocumentPrivate {
	JsonObject *root_object;
	CouchdbDatabase *database;
};

G_DEFINE_TYPE(CouchdbDocument, couchdb_document, G_TYPE_OBJECT);

enum {
	PROP_0,
	PROP_DATABASE
};

static void
couchdb_document_finalize (GObject *object)
{
	CouchdbDocument *document = COUCHDB_DOCUMENT (object);

	if (document->priv != NULL) {
		if (document->priv->database != NULL)
			g_object_unref (G_OBJECT (document->priv->database));

		json_object_unref (document->priv->root_object);
		g_free (document->priv);
	}

	G_OBJECT_CLASS (couchdb_document_parent_class)->finalize (object);
}

static void
couchdb_document_set_property (GObject *object,
			       guint prop_id,
			       const GValue *value,
			       GParamSpec *pspec)
{
	CouchdbDocument *document = COUCHDB_DOCUMENT (object);

	switch (prop_id) {
	case PROP_DATABASE:
		if (document->priv->database != NULL)
			g_object_unref (G_OBJECT (document->priv->database));
		document->priv->database = g_value_dup_object (value);
		break;
	default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
	}
}

static void
couchdb_document_get_property (GObject *object,
                               guint prop_id,
                               GValue *value,
                               GParamSpec *pspec)
{
	CouchdbDocument *document = COUCHDB_DOCUMENT (object);

	switch (prop_id) {
	case PROP_DATABASE:
		g_value_set_object (value, document->priv->database);
		break;
	default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
                break;
	}
}

static void
couchdb_document_class_init (CouchdbDocumentClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = couchdb_document_finalize;
	object_class->set_property = couchdb_document_set_property;
	object_class->get_property = couchdb_document_get_property;

	g_object_class_install_property (object_class,
					 PROP_DATABASE,
					 g_param_spec_object ("database",
							      "Database object",
							      "Database from which this document was retrieved/saved",
							      COUCHDB_TYPE_DATABASE,
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
}

static void
couchdb_document_init (CouchdbDocument *document)
{
	document->priv = g_new0 (CouchdbDocumentPrivate, 1);
	document->priv->root_object = json_object_new ();
}

/**
 * couchdb_document_new:
 *
 * Create an empty #CouchdbDocument object, which can then be populated with data
 * and added to a database.
 *
 * Return value: A newly-created #CouchdbDocument object, with no data on it.
 */
CouchdbDocument *
couchdb_document_new (void)
{
	CouchdbDocument *document;

	document = g_object_new (COUCHDB_TYPE_DOCUMENT, NULL);

	return document;
}

CouchdbDocument *
couchdb_document_new_from_json_object (JsonObject *json_object)
{
	CouchdbDocument *document;

	document = g_object_new (COUCHDB_TYPE_DOCUMENT, NULL);

	if (document->priv->root_object != NULL)
		json_object_unref (document->priv->root_object);

	document->priv->root_object = json_object_ref (json_object);

	return document;
}

void
couchdb_document_set_from_json_object (CouchdbDocument *document, JsonObject *json_object)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));

	if (document->priv->root_object != NULL)
		json_object_unref (document->priv->root_object);

	document->priv->root_object = json_object_ref (json_object);
}


/**
 * couchdb_document_get_id:
 * @document: A #CouchdbDocument object
 *
 * Retrieve the unique ID of the given document.
 *
 * Return value: The unique ID of the given document.
 */
const char *
couchdb_document_get_id (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	if (document->priv->root_object != NULL) {
		if (json_object_has_member (document->priv->root_object, "_id"))
			return json_object_get_string_member (
				document->priv->root_object,
				"_id");
	}

	return NULL;
}

/**
 * couchdb_document_set_id:
 * @document: A #CouchdbDocument object
 * @id: New unique ID for the given document.
 *
 * Set the unique ID for a given document.
 *
 * This usually is not needed by calling applications, unless they want to
 * force IDs on the CouchDB documents created/updated for some reason, like
 * compatibility with 3rd party applications. In most cases, the autogenerated
 * IDs from CouchDB when new documents are created (see #couchdb_document_put)
 * should be ok for most applications.
 */
void
couchdb_document_set_id (CouchdbDocument *document, const char *id)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (id != NULL);

	json_object_set_string_member (document->priv->root_object, "_id", id);
}

/**
 * couchdb_document_get_revision:
 * @document: A #CouchdbDocument object
 *
 * Retrieve the revision of a given document.
 *
 * CouchDB does not overwrite updated documents in place, instead it creates a
 * new document at the end of the database file, with the same ID but a new revision.
 *
 * Document revisions are used for optimistic concurrency control and applications
 * should not rely on document revisions for any other purpose than concurrency control.
 *
 * Return value: Revision of the given document.
 */
const char *
couchdb_document_get_revision (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	if (document->priv->root_object != NULL) {
		return json_object_get_string_member (
			document->priv->root_object,
			"_rev");
	}

	return NULL;
}

/**
 * couchdb_document_set_revision:
 * @document: A #CouchdbDocument object
 * @revision: String specifying the revision to set the document to
 *
 * Set the revision of the given document. This should never be used by applications,
 * unless they really know what they are doing, since using a wrong revision string
 * will confuse CouchDB when doing updates to the document.
 */
void
couchdb_document_set_revision (CouchdbDocument *document, const char *revision)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (revision != NULL);

	json_object_set_string_member (document->priv->root_object, "_rev", revision);
}

/**
 * couchdb_document_has_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to check existence for in the document
 *
 * Check whether the given document has a field with a specific name.
 *
 * Return value: TRUE if the field exists in the document, FALSE if not.
 */
gboolean
couchdb_document_has_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);
	g_return_val_if_fail (field != NULL, FALSE);

	return json_object_has_member (document->priv->root_object, field);
}

/**
 * couchdb_document_remove_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to remove from the document
 *
 * Remove a field from the given document.
 */
void
couchdb_document_remove_field (CouchdbDocument *document, const char *field)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);

	json_object_remove_member (document->priv->root_object, field);
}

/**
 * couchdb_document_get_field_names:
 * @document: A #CouchdbDocument object
 *
 * Retrieve the list of fields contained in the given #CouchdbDocument.
 *
 * Returns: (element-type utf8) (transfer full): A list of strings containing the names of all the fields contained
 * in the given #CouchdbDocument object. When no longer needed, the list should
 * be freed by calling #g_slist_free.
 */
GSList *
couchdb_document_get_field_names (CouchdbDocument *document)
{
	GList *json_list;
	GSList *result = NULL;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	json_list = json_object_get_members (document->priv->root_object);
	while (json_list != NULL) {
		result = g_slist_append (result, json_list->data);

		json_list = g_list_remove (json_list, json_list->data);
	}

	return result;
}

/**
 * couchdb_document_get_field_type:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to get type
 *
 * Get the value type of the given #CouchdbDocument's field.
 *
 * Return value: Type of the field.
 */
GType
couchdb_document_get_field_type (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), G_TYPE_INVALID);

	return get_type_from_field (document->priv->root_object, field);
}

/**
 * couchdb_document_get_array_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of an array field from the given document.
 *
 * Returns: (transfer full): The value of the given array field.
 */
CouchdbArrayField *
couchdb_document_get_array_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);
	g_return_val_if_fail (field != NULL, NULL);

	if (!json_object_has_member (document->priv->root_object, field))
		return NULL;

	return couchdb_array_field_new_from_json_array (
		json_array_ref (json_object_get_array_member (document->priv->root_object,
							      field)));
}

/**
 * couchdb_document_set_array_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of an array field in the given document.
 */
void
couchdb_document_set_array_field (CouchdbDocument *document, const char *field, CouchdbArrayField *value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);
	g_return_if_fail (value != NULL);

	json_object_set_array_member (document->priv->root_object,
				      field,
				      json_array_ref (couchdb_array_field_get_json_array (value)));
}

/**
 * couchdb_document_get_boolean_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of a boolean field from the given document.
 *
 * Return value: The value of the given boolean field.
 */
gboolean
couchdb_document_get_boolean_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);
	g_return_val_if_fail (field != NULL, FALSE);

	if (!json_object_has_member (document->priv->root_object, field))
		return FALSE;

	return json_object_get_boolean_member (document->priv->root_object,
					       field);
}

/**
 * couchdb_document_set_boolean_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of a boolean field in the given document.
 */
void
couchdb_document_set_boolean_field (CouchdbDocument *document, const char *field, gboolean value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);

	json_object_set_boolean_member (document->priv->root_object, field, value);
}

/**
 * couchdb_document_get_int_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of an integer field from the given document.
 *
 * Return value: The value of the given integer field.
 */
gint
couchdb_document_get_int_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), -1);
	g_return_val_if_fail (field != NULL, -1);

	if (!json_object_has_member (document->priv->root_object, field))
		return 0;

	return json_object_get_int_member (document->priv->root_object,
					   field);
}

/**
 * couchdb_document_set_int_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of an integer field in the given document.
 */
void
couchdb_document_set_int_field (CouchdbDocument *document, const char *field, gint value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);

	json_object_set_int_member (document->priv->root_object, field, value);
}

/**
 * couchdb_document_get_double_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of a decimal number field from the given document.
 *
 * Return value: The value of the given decimal number field.
 */
gdouble
couchdb_document_get_double_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), -1);
	g_return_val_if_fail (field != NULL, -1);

	if (!json_object_has_member (document->priv->root_object, field))
		return 0.0;

	return json_object_get_double_member (document->priv->root_object, field);
}

/**
 * couchdb_document_set_double_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of a decimal number field in the given document.
 */
void
couchdb_document_set_double_field (CouchdbDocument *document, const char *field, gdouble value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);

	json_object_set_double_member (document->priv->root_object, field, value);
}

/**
 * couchdb_document_get_string_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of a string field from the given document.
 *
 * Return value: The value of the given string field.
 */
const char *
couchdb_document_get_string_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);
	g_return_val_if_fail (field != NULL, NULL);

	if (!json_object_has_member (document->priv->root_object, field))
		return NULL;

	return json_object_get_string_member (document->priv->root_object, field);
}

/**
 * couchdb_document_set_string_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of a string field in the given document.
 */
void
couchdb_document_set_string_field (CouchdbDocument *document, const char *field, const char *value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);

	if (value) {
		json_object_set_string_member (document->priv->root_object,
					       field,
					       value);
	} else {
		/* Remove field if it's a NULL value */
		couchdb_document_remove_field (document, field);
	}
}

/**
 * couchdb_document_get_struct_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to retrieve
 *
 * Retrieve the value of a struct field from the given document.
 *
 * Returns: (transfer full): The value of the given struct field.
 */
CouchdbStructField *
couchdb_document_get_struct_field (CouchdbDocument *document, const char *field)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);
	g_return_val_if_fail (field != NULL, NULL);

	if (!json_object_has_member (document->priv->root_object, field))
		return NULL;

	return couchdb_struct_field_new_from_json_object (
		json_object_ref (json_object_get_object_member (document->priv->root_object,
								field)));
}

/**
 * couchdb_document_set_struct_field:
 * @document: A #CouchdbDocument object
 * @field: Name of the field to set
 * @value: Value to set the field to
 *
 * Set the value of a struct field in the given document.
 */
void
couchdb_document_set_struct_field (CouchdbDocument *document, const char *field, CouchdbStructField *value)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (field != NULL);
	g_return_if_fail (value != NULL);

	json_object_set_object_member (document->priv->root_object,
				       field,
				       json_object_ref (couchdb_struct_field_get_json_object (value)));
}

/**
 * couchdb_document_list_attachments:
 * @document: A #CouchdbDocument object
 *
 * List all the attachments for the given document.
 *
 * Returns: (element-type utf8) (transfer full): A list containing the IDs of all the attachments for the
 * given document. When no longer needed, the list should
 * be freed by calling #g_slist_free.
 */
GSList *
couchdb_document_list_attachments (CouchdbDocument *document)
{
	GSList *list = NULL;
	CouchdbStructField *attachments;

	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	attachments = couchdb_document_get_struct_field (document, "_attachments");
	if (attachments != NULL) {
		list = couchdb_struct_field_get_field_names (attachments);

		g_object_unref (G_OBJECT (attachments));
	}

	return list;
}

/**
 * couchdb_document_to_string:
 * @document: A #CouchdbDocument object
 *
 * Convert the given #CouchdbDocument to a JSON string.
 *
 * Return value: A string containing the given document in JSON format.
 */
char *
couchdb_document_to_string (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	if (document->priv->root_object) {
		JsonGenerator *generator;
		char *str;
		gsize size;
		JsonNode *root_node;

		generator = json_generator_new ();
		root_node = json_node_new (JSON_NODE_OBJECT);
		json_node_set_object (root_node, json_object_ref (document->priv->root_object));
		json_generator_set_root (generator, root_node);

		str = json_generator_to_data (generator, &size);
		g_object_unref (G_OBJECT (generator));
		json_node_free (root_node);

		return str;
	}

	return NULL;
}

JsonObject *
couchdb_document_get_json_object (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);
	
	return document->priv->root_object;
}

/**
 * couchdb_document_get_record_type:
 * @document: A #CouchdbDocument object
 *
 * Retrieve the record type of the given document. Record types are special
 * fields in the CouchDB documents, used in Desktopcouch, to identify
 * standard records. All supported record types are listed at
 * http://www.freedesktop.org/wiki/Specifications/desktopcouch#Formats.
 *
 * Return value: The record type of the given document.
 */
const char *
couchdb_document_get_record_type (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	return couchdb_document_get_string_field (COUCHDB_DOCUMENT (document), "record_type");
}

/**
 * couchdb_document_set_record_type:
 * @document: A #CouchdbDocument object
 * @record_type: Record type to set the document to
 *
 * Set the record type of the given document.
 */
void
couchdb_document_set_record_type (CouchdbDocument *document, const char *record_type)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));
	g_return_if_fail (record_type != NULL);

	couchdb_document_set_string_field (COUCHDB_DOCUMENT (document), "record_type", record_type);
}

/**
 * couchdb_document_get_application_annotations:
 * @document: A #CouchdbDocument object
 *
 * Retrieve the application annotations for the given document.
 *
 * Application annotations is a special field (named "application_annotations"), used
 * in Desktopcouch to allow applications to set values on standard documents (as defined
 * at http://www.freedesktop.org/wiki/Specifications/desktopcouch#Formats) that are
 * not part of the standard, but still needed by the application.
 *
 * Returns: (transfer full): A #CouchdbStructField containing the value of the application
 * annotations for the given document.
 */
CouchdbStructField *
couchdb_document_get_application_annotations (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), NULL);

	return couchdb_document_get_struct_field (COUCHDB_DOCUMENT (document), "application_annotations");
}

/**
 * couchdb_document_set_application_annotations:
 * @document: A #CouchdbDocument object
 * @annotations: A #CouchdbStructField with the contents of the application_annotations field.
 *
 * Set the application annotations for the given document.
 */
void
couchdb_document_set_application_annotations (CouchdbDocument *document, CouchdbStructField *annotations)
{
	g_return_if_fail (COUCHDB_IS_DOCUMENT (document));

	couchdb_document_set_struct_field (COUCHDB_DOCUMENT (document), "application_annotations", annotations);
}

/**
 * couchdb_document_is_contact:
 * @document: A #CouchdbDocument object
 *
 * Check whether the given document represents a contact record, as specified
 * at http://www.freedesktop.org/wiki/Specifications/desktopcouch/contact
 *
 * Return value: TRUE if the document represents a contact, FALSE otherwise.
 */
gboolean
couchdb_document_is_contact (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);

	return !g_strcmp0 (couchdb_document_get_record_type (document),
			   COUCHDB_RECORD_TYPE_CONTACT);
}

/**
 * couchdb_document_is_task:
 * @document: A #CouchdbDocument object
 *
 * Check whether the given document represents a contact record, as specified
 * at http://www.freedesktop.org/wiki/Specifications/desktopcouch/task
 *
 * Return value: TRUE if the document represents a task, FALSE otherwise.
 */
gboolean
couchdb_document_is_task (CouchdbDocument *document)
{
	g_return_val_if_fail (COUCHDB_IS_DOCUMENT (document), FALSE);

	return !g_strcmp0 (couchdb_document_get_record_type (document),
			   COUCHDB_RECORD_TYPE_TASK);
}
