/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Miguel Angel Rodelas Delgado <miguel.rodelas@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __COUCHDB_DOCUMENT_TASK_H__
#define __COUCHDB_DOCUMENT_TASK_H__

#include <couchdb-struct-field.h>
#include <couchdb-document.h>

G_BEGIN_DECLS

#define COUCHDB_TYPE_DOCUMENT_TASK                (couchdb_document_task_get_type ())
#define COUCHDB_DOCUMENT_TASK(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), COUCHDB_TYPE_DOCUMENT_TASK, CouchdbDocumentTask))
#define COUCHDB_IS_DOCUMENT_TASK(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), COUCHDB_TYPE_DOCUMENT_TASK))
#define COUCHDB_DOCUMENT_TASK_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), COUCHDB_TYPE_DOCUMENT_TASK, CouchdbDocumentTaskClass))
#define COUCHDB_IS_DOCUMENT_TASK_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), COUCHDB_TYPE_DOCUMENT_TASK))
#define COUCHDB_DOCUMENT_TASKGET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), COUCHDB_TYPE_DOCUMENT_TASK, CouchdbDocumentTaskClass))

typedef struct {
	CouchdbDocument parent;
} CouchdbDocumentTask;

typedef struct {
	CouchdbDocumentClass parent_class;
} CouchdbDocumentTaskClass;

GType       couchdb_document_task_get_type (void);

CouchdbDocumentTask *couchdb_document_task_new (void);

const char *couchdb_document_task_get_summary (CouchdbDocumentTask *document);
void        couchdb_document_task_set_summary (CouchdbDocumentTask *document, const char *summary);

const char *couchdb_document_task_get_description (CouchdbDocumentTask *document);
void        couchdb_document_task_set_description (CouchdbDocumentTask *document, const char *description);

G_END_DECLS

#endif /* __COUCHDB_DOCUMENT_TASK_H__ */
