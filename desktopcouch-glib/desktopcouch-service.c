/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2011 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <sys/wait.h>
#include <glib-object.h>
#include "desktopcouch-service.h"

struct _DesktopcouchServicePrivate {
	gchar *instance_name;
	GPid pid;
};

G_DEFINE_TYPE(DesktopcouchService, desktopcouch_service, G_TYPE_OBJECT)

#define GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), DESKTOPCOUCH_TYPE_SERVICE, DesktopcouchServicePrivate))

static void
desktopcouch_service_finalize (GObject *object)
{
	DesktopcouchService *service = DESKTOPCOUCH_SERVICE (object);

	if (service->priv != NULL) {
		if (service->priv->instance_name)
			g_free (service->priv->instance_name);

		g_spawn_close_pid (service->priv->pid);
	}
	
	G_OBJECT_CLASS (desktopcouch_service_parent_class)->finalize (object);
}

static void
desktopcouch_service_class_init (DesktopcouchServiceClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = desktopcouch_service_finalize;
}

static void
desktopcouch_service_init (DesktopcouchService *service)
{
	service->priv = GET_PRIVATE (service);
}

static void
couchdb_watch_cb (GPid pid, gint status, gpointer user_data)
{
	DesktopcouchService *service = DESKTOPCOUCH_SERVICE (user_data);

	if (WIFEXITED (status)) {
		g_print ("Process %d has exited with code %d\n", pid, WEXITSTATUS (status));
	} else if (WIFSIGNALED (status)) {
		g_print ("Process %d received signal %d\n", pid, WTERMSIG (status));
	} else if (WIFSTOPPED (status)) {
		g_print ("Process %d was stopped with signal %d\n", pid, WSTOPSIG (status));
	}
}

static void
spawn_couchdb (DesktopcouchService *service)
{
	gchar *couchdb_args[7], *data_dir;
	GError *error = NULL;

	/* Spawn CouchDB process */
	couchdb_args[0] = "couchdb";
	couchdb_args[1] = "-a";
	couchdb_args[2] = g_build_filename (g_get_user_config_dir (), service->priv->instance_name, "desktopcouch.ini", NULL);
	couchdb_args[3] = "-b";
	couchdb_args[4] = "-p";
	couchdb_args[5] = g_build_filename (g_get_user_cache_dir (), service->priv->instance_name,  "desktopcouch.pid", NULL);
	couchdb_args[6] = NULL;

	data_dir = g_build_filename (g_get_user_data_dir (), service->priv->instance_name, NULL);

	if (g_spawn_async (data_dir,
			   couchdb_args,
			   NULL,
			   G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
			   NULL,
			   NULL,
			   &service->priv->pid,
			   &error)) {
		gchar *contents;
		gsize size;

		/* Read the PID file to get the process ID to watch */
		error = NULL;
		if (g_file_get_contents (couchdb_args[5], &contents, &size, &error)) {
			service->priv->pid = atoi (contents);
			g_child_watch_add (service->priv->pid, (GChildWatchFunc) couchdb_watch_cb, service);

			g_free (contents);
		}
	} else {
		g_print ("Error spawning couchdb: %s\n", error->message);
		g_error_free (error);
	}

	/* Free memory */
	g_free (couchdb_args[2]);
	g_free (couchdb_args[5]);
	g_free (data_dir);
}

/**
 * desktopcouch_service_new:
 * @name: Name to use for this instance of the service.
 *
 * Create a new #DesktopcouchService object, which is a wrapper that makes it possible
 * to run an instance of a CouchDB server for the current logged in user.
 *
 * Return value: A #DesktopcouchService instance.
 */
DesktopcouchService *
desktopcouch_service_new (const gchar *name)
{
	GDBusConnection *bus;
	GError *error;
	DesktopcouchService *service;

	/* First of all, make sure we own the DBus service */
	error = NULL;
	bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
	if (error) {
		g_warning ("Couldn't get session bus: %s", error->message);
		g_error_free (error);

		return NULL;
	}

	/* Create object */
	service = g_object_new (DESKTOPCOUCH_TYPE_SERVICE, NULL);
	service->priv->instance_name = g_strdup (name);

	g_idle_add ((GSourceFunc) spawn_couchdb, service);

	return service;
}
