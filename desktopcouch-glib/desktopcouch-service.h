/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2011 Canonical Services Ltd (www.canonical.com)
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __DESKTOPCOUCH_SERVICE_H__
#define __DESKTOPCOUCH_SERVICE_H__

#include <couchdb-glib.h>

G_BEGIN_DECLS

#define DESKTOPCOUCH_TYPE_SERVICE                (desktopcouch_service_get_type ())
#define DESKTOPCOUCH_SERVICE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), DESKTOPCOUCH_TYPE_SERVICE, DesktopcouchService))
#define DESKTOPCOUCH_IS_SERVICE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DESKTOPCOUCH_TYPE_SERVICE))
#define DESKTOPCOUCH_SERVICE_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), DESKTOPCOUCH_TYPE_SERVICE, DesktopcouchServiceClass))
#define DESKTOPCOUCH_IS_SERVICE_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), DESKTOPCOUCH_TYPE_SERVICE))
#define DESKTOPCOUCH_SERVICE_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), DESKTOPCOUCH_TYPE_SERVICE, DesktopouchServiceClass))

typedef struct _DesktopcouchServicePrivate DesktopcouchServicePrivate;

typedef struct {
	GObject parent;
	DesktopcouchServicePrivate *priv;
} DesktopcouchService;

typedef struct {
	GObjectClass parent_class;
} DesktopcouchServiceClass;

GType                desktopcouch_service_get_type (void);
DesktopcouchService *desktopcouch_service_new (const gchar *name);

G_END_DECLS

#endif
